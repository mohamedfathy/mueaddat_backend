<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\CommonHelper;
use App\Models\Ad;
use App\Models\Admin;
use App\Models\AppSetting;
use App\Models\BalanceRecharge;
use App\Models\BankAccount;
use App\Models\BeautyCenter;
use App\Models\BeautyImage;
use App\Models\BeautyPhone;
use App\Models\BeautyService;
use App\Models\Category;
use App\Models\Contact;
use App\Models\DiscountCode;
use App\Models\Email;
use App\Models\Favorite;
use App\Models\Location;
use App\Models\Notification;
use App\Models\NotifyUser;
use App\Models\Order;
use App\Models\OrderBeautyService;
use App\Models\Phone;
use App\Models\Provider;
use App\Models\Review;
use App\Models\Session;
use App\Models\ShopLevel;
use App\Models\UsedCode;
use App\User;


class APIsHelper extends Controller
{
    public static function IsFavouriteShop ($ApiToken, $ShopId) 
    {
        $Account = CommonHelper::GetAccountObject($ApiToken, null, null, null);
        if($Account == null) {return false;}
        $Favorite = Favorite::where('beauty_centers_id', $ShopId)
                            ->where('users_id', $Account->id)
                            ->first();
        return $Favorite !== null ? true : false;
    }
    
    public static function IsDiscountedOrder ($OrderId) 
    {
        $UsedCode = UsedCode::where('orders_id', $OrderId)->first();
        return is_null($UsedCode) ? false : true;
    }
}