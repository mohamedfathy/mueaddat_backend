<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Validator;
use Hash;
use App\Models\Ad;
use App\Models\Admin;
use App\Models\AppSetting;
use App\Models\BalanceRecharge;
use App\Models\BankAccount;
use App\Models\BeautyCenter;
use App\Models\BeautyImage;
use App\Models\BeautyPhone;
use App\Models\BeautyService;
use App\Models\Category;
use App\Models\Contact;
use App\Models\DiscountCode;
use App\Models\Email;
use App\Models\Favorite;
use App\Models\Location;
use App\Models\Notification;
use App\Models\NotifyUser;
use App\Models\Order;
use App\Models\OrderBeautyService;
use App\Models\Phone;
use App\Models\Provider;
use App\Models\Review;
use App\Models\Session;
use App\Models\ShopLevel;
use App\Models\UsedCode;
use App\User;
use Carbon;

class CommonHelper 
{
    public static $PerPage = 20; 
    public static $StaticXDigits = 123456;

    public static function Validate ($Request, $Rules, $Messages) 
    {
        $validator = Validator::make($Request, $Rules, $Messages);
        //dd($validator->errors());
        if ($validator->fails()) {
            return response()->json(['status' => (int) $validator->errors()->first(), 
            'error' => $validator->errors()
            ]);
        }
    }
    
    public static function HasItems ($Collection) 
    {
        if ($Collection === null) {return false;}
        if(count($Collection) === 0) {return false;} 
        else {return true;} 
    }
    
    public static function BaseSixtyFourImage ($String, $Name) 
    {
        $URL = $Name."_".time()."_".str_random(10).'.' . explode('/', explode(':', substr($String, 0, strpos($String, ';')))[1])[1];
        $path = public_path().'/uploads/'.$Name.'/' . $URL;
        $binary = file_get_contents($String);
        $success = file_put_contents($path, $binary);
        $ImageUrl = '/uploads/'.$Name.'/'.$URL;
        return $ImageUrl;
    }

    public static function Response ($Code) 
    {
        return response()->json(['status' => $Code]);
    }

    public static function UploadFile ($File, $Directory="temp") 
    {
        $FileName = str_random(30) . '.' . $File->getClientOriginalExtension();
        $File->move(public_path("uploads/{$Directory}") , $FileName);
        return "/uploads/{$Directory}/".$FileName;
    }

    public static function GetNearest ($Longtiude, $Latitude, $Distance=10) 
    {
        return Location::select(DB::raw('*, ( 6367 * acos( cos( radians('.$Latitude.') ) * cos( radians( lotitude ) ) * cos( radians( longitude ) - radians('.$Longtiude.') ) + sin( radians('.$Latitude.') ) * sin( radians( lotitude ) ) ) ) AS distance'))
                                    ->having('distance', '<', $Distance)
                                    ->orderBy('distance')
                                    ->get();
    }
    
    public static function SetIsSeen ($Notifications) 
    { 
        foreach ($Notifications as $Notification) {
            $Notification->is_seen = 1;
            $Notification->save();
        }
    }

    //Pagination 
    public static function ResourcesToSkip ($page) 
    {
        return self::$PerPage * $page;
    }

    public static function IsAccountActive ($ApiToken=NULL, $Phone=NULL, $Email=NULL, $Id=NULL) 
    {
        $Key =""; $Value="";
        if ($ApiToken !== NULL) {$Key = 'api_token';   $Value = $ApiToken;} 
        if ($Phone    !== NULL) {$Key = 'phone';       $Value = $Phone;} 
        if ($Email    !== NULL) {$Key = 'email';       $Value = $Email;} 
        if ($Id       !== NULL) {$Key = 'id';          $Value = $Id;} 
        $Account = User::where($Key, $Value)->where('is_active', 1)->first();
        if($Account !== null) {return $Account;}
        $Account = Provider::where($Key, $Value)->where('is_active', 1)->first();
        if($Account !== null) {return $Account;}
        
        // return Account Object Or Null
        return null;
    }

    public static function IsAccountVerified ($ApiToken=NULL, $Phone=NULL, $Email=NULL, $Id=NULL) 
    {
        $Key =""; $Value="";
        if ($ApiToken !== NULL) {$Key = 'api_token';   $Value = $ApiToken;} 
        if ($Phone    !== NULL) {$Key = 'phone';       $Value = $Phone;} 
        if ($Email    !== NULL) {$Key = 'email';       $Value = $Email;} 
        if ($Id       !== NULL) {$Key = 'id';          $Value = $Id;} 

        $Account = User::where($Key, $Value)->where('is_verified', 1)->first();
        if($Account !== null) {return $Account;}
        $Account = Provider::where($Key, $Value)->where('is_verified', 1)->first();
        if($Account !== null) {return $Account;}
        
        // return Account Object Or Null
        return null;
    }

    public static function IsAccountValid ($ApiToken=NULL, $Phone=NULL, $Email=NULL, $Id=NULL) 
    {
        $Key =""; $Value="";
        if ($ApiToken !== NULL) {$Key = 'api_token';   $Value = $ApiToken;} 
        if ($Phone    !== NULL) {$Key = 'phone';       $Value = $Phone;} 
        if ($Email    !== NULL) {$Key = 'email';       $Value = $Email;} 
        if ($Id       !== NULL) {$Key = 'id';          $Value = $Id;} 
        // Table to check on
        $Account = User::where($Key, $Value)->where('is_active', 1)->where('is_verified', 1)->first();
        if($Account !== null) {return $Account;}
        $Account = Provider::where($Key, $Value)->where('is_active', 1)->where('is_verified', 1)->first();
        if($Account !== null) {return $Account;}
        
        // return Account Object Or Null
        return null;
    }

    public static function GetAccountObject ($ApiToken=NULL, $Phone=NULL, $Email=NULL, $Id=NULL) 
    {
        //$Key =""; $Value="";
        if ($ApiToken !== NULL) {$Key = 'api_token';   $Value = $ApiToken;} 
        if ($Phone    !== NULL) {$Key = 'phone';       $Value = $Phone;} 
        if ($Email    !== NULL) {$Key = 'email';       $Value = $Email;} 
        if ($Id       !== NULL) {$Key = 'id';          $Value = $Id;} 
        // Table to check on
        $Account = User::where($Key, $Value)->first();
        if($Account !== null) {return $Account;}
        $Account = Provider::where($Key, $Value)->first();
        if($Account !== null) {return $Account;}
        
        // return AccountID Or Null
        return null;
    }
    
    public static function GetAccountObjectById ($Id=NULL, $Type=NULL) 
    {
        // Table to check on
        if($Type == 'User') {
            $Account = User::where('id', $Id)->first();
            if($Account !== null) {return $Account;}
        }
        if($Type == 'Provider') {
            $Account = Provider::where('id', $Id)->first();
            if($Account !== null) {return $Account;}
        }
        
        // return AccountID Or Null
        return null;
    }

    public static function CheckSession ($TmpToken=NULL, $TmpPhone=NULL, $TmpEmail=NULL, $UserId=NULL) 
    {
        $Key =""; $Value="";
        if ($TmpToken !== NULL) {$Key = 'tmp_token';       $Value = $TmpToken;} 
        if ($TmpPhone !== NULL) {$Key = 'tmp_phone';       $Value = $TmpPhone;} 
        if ($TmpEmail !== NULL) {$Key = 'tmp_email';       $Value = $TmpEmail;} 
        if ($UserId   !== NULL) {$Key = 'user_id';         $Value = $UserId;} 
        // Table to check on
        $Session = Session::where($Key, $Value)->first();
        // return AccountID Or Null
        return $Session === NULL ? NULL : $Session;
    }
    
    public static function CheckSessionById ($AccountType=NULL, $AccountId=NULL) 
    {
        if($AccountType == 'User') {
            $Session = Session::where('users_id', $AccountId)->first();
            if($Session !== null) {return $Session;}
        }
        if($AccountType == 'Provider') {
            $Session = Session::where('providers_id', $AccountId)->first();
            if($Session !== null) {return $Session;}
        }
        return null;
    }

    public static function StoreSession ($Code, $TmpToken=NULL, $TmpPhone=NULL, $UserId=NULL,  $PersonId=NULL,  $AgencyId=NULL,  $CompanyId=NULL) 
    {
        $Session = new Session();
        $Session->tmp_token      = $TmpToken;
        $Session->tmp_phone      = $TmpPhone;
        $Session->code           = $Code;
        $Session->usesrs_id      = $UserId;
        $Session->persons_id     = $PersonId;
        $Session->agencies_id    = $AgencyId;
        $Session->companies_id   = $CompanyId;
        $Session->created_at     = date('Y-m-d H:i:s');
        $Session->save();
    }

    public static function UpdateSession ($TmpToken=NULL, $TmpPhone=NULL, $TmpEmail=NULL, $UserId=NULL) 
    {
        if ($TmpToken    !== NULL) {$Key = 'tmp_token';       $Value = $TmpToken;} 
        if ($TmpPhone    !== NULL) {$Key = 'tmp_phone';       $Value = $TmpPhone;} 
        if ($TmpEmail    !== NULL) {$Key = 'tmp_email';       $Value = $TmpEmail;} 
        if ($UserId      !== NULL) {$Key = 'user_id';         $Value = $UserId;} 
        // Table to check on
        $Session = Session::where($Key, $Value)->first();
        if($Session === NULL) { return $Session;}
        $Session->created_at = date('Y-m-d H:i:s'); 
        $Session->save();
        
        return $Session;
    }

    public static function DeleteSession ($SessionId) 
    {
        Session::destroy($SessionId);
    }

    public static function HashPassword ($Password) 
    {
        return Hash::make($Password);
    }

    public static function RandomXDigits ($Digits=6) 
    {
        return rand(pow(10, $Digits-1), pow(10, $Digits)-1);
    }

    public static function RandomXChars ($Chars=69) 
    {
        return str_random($Chars);
    }
    
    public static function GetTableName ($Collection) 
    {
        return $Collection->getTable();
    }
    
    public static function DiscountAccountFees ($Account, $Type, $Price) 
    {
        // Get fees from user accout or appSetting.
        $Fees = !is_null($Account->product_fees) ? $Account->fees : self::ApplicationFees($Type);
        // Get Discount value and Cutoff Balance
        $discount_amount   = ($Fees / 100) * $Price;
        $Account->mybalance = $Account->mybalance - $discount_amount;
        $Account->save();
        return $discount_amount;
    }
    
    public static function ApplicationFees ($Type) 
    {
        // Get fees Appliaction Setting.
        if ($Type == 'Product')     { $Fees = AppSetting::first()->product_fees;}
        if ($Type == 'Maintenance') {  $Fees = AppSetting::first()->maintenance_fees;}
        return $Fees;
    }
    
    public static function RegenerateAPIToken ($Account) 
    {
        $Account->api_token = self::RandomXChars(69);
        $Account->save();
    }
    
    public static function GetAccountLanguage ($UserId, $ProviderId) 
    {
        if ($UserId    != null)  { $Account    =  User::find($UserId);}
        if ($ProviderId  != null)  { $Account    =  Provider::find($ProviderId);}
        
        return $Account->language;
    }
    
    public static function GetAppSetting () 
    {
        return AppSetting::first();
    }
    
    public static function CheckForBalance ($Id, $Balance) 
    {
        $User = User::find($Id);
        return $User->balance > $Balance ? true : false;
    }
    
    // Sms Helper
    public static function SendSMS ($Phone=null, $Message=null) 
    {
        $client = new Client(); 
        $result = $client->post('https://www.ksa-sms.com/api/sendsms.php', [
            'form_params' => [
                'username'     => 'Leader',
                "password"    => "Le73766061",
                "message"        =>  $Message,
                "sender"        => "Le3der-ad",
                "numbers"        => $Phone,
                "unicode"     => "E",
                "return"        => "full",
            ]
        ]);
        $result->getBody()->getContents();
    }
    
    // Get Current DateTime For mysql
    public static function GetDateTime () 
    {
        return Carbon::now('+2:00')->format('Y-m-d H:i:s');
        //return date("Y-m-d H:i:s"); 
    }
    // Get Current Date
    public static function GetDate () 
    {
        return Carbon::now('+2:00')->format('Y-m-d');
        //return date("Y-m-d"); 
    }
    // Get Current Time
    public static function GetTime () 
    {
        return Carbon::now('+2:00')->format('H:i:s');
    }

    // Get Current Time
    public static function GetTimeStamp () 
    {
        // 7200 is added to set timezone +2:00
        return Carbon::now()->timestamp + 7200;

    }
    // Convert TimeStamp To DateTime
    public static function ConvertToDateTime ($TimeStamp) 
    {
        return date("Y-m-d H:i:s", $TimeStamp);
    }
    // Convert TimeStamp To Date
    public static function ConvertToDate ($TimeStamp) 
    {
        return date("Y-m-d", $TimeStamp);
    }
    // Convert TimeStamp To Time
    public static function ConvertToTime ($TimeStamp) 
    {
        return date("H:i:s", $TimeStamp);
    }
    // Convert string DateTime to Timestamp
    public static function ConvertToTimeStamp ($String) 
    {
        return strtotime($String);
    }
    // Format TimeStamp 
    public static function FormatTimeStamp ($TimeStamp) 
    {
        return strftime("%A, %d	%b %Y At %I:%M	%p", $TimeStamp);
    }
    // Format DateTime
    public static function FormatString ($String) 
    {
        $TimeStamp = self::ConvertToTimeStamp($String);
        return self::FormatTimeStamp($TimeStamp);
    }

    // public static function AddDate ($DateTime, $minutesToAdd) 
    // {
    //     $addedDateTime = date('Y-m-d H:i:s', strtotime("+{$minutesToAdd} minutes",strtotime($DateTime)));
    //     return $addedDateTime;
    // }

    public static function MinutesToTimeStamp ($Minutes) 
    {
        return $Minutes * 60;
    }

    public static function IsTimeOver ($String, $Minutes) 
    {
        $FirstTimeStamp = self::ConvertToTimeStamp($String);
        $SecondTimeStamp = self::MinutesToTimeStamp($Minutes);

        $AllowedTimeStamp = $FirstTimeStamp + $SecondTimeStamp; 
        $CurrentTimeStamp = self::GetTimeStamp();

        return $CurrentTimeStamp > $AllowedTimeStamp  ? true : false;
    }

    
}