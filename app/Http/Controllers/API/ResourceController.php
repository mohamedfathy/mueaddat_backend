<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\CommonHelper;
use App\Http\Controllers\API\APIsHelper;
use App\Models\Billing;
use App\Models\Brand;
use App\Models\Featured;
use App\Models\Category;
use App\Models\Option;
use App\Models\OptionGroup;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Payment;
use App\Models\Product;
use App\Models\ProductDetail;
use App\Models\ProductImage;
use App\Models\ProductOption;
use App\Models\ProductReview;
use App\Models\Slider;
use App\Models\Shipping;
use App\Models\ShippingDetail;
use App\Models\Supplier;

class ResourceController extends Controller
{
    public static function ResourceArray ($ResourceName, $Records) 
    {
        global $request;
        // fetch all resources
        $Resources = [];
        //dd($Records);
        foreach ($Records as $Record) {
            $Resources[] = self::$ResourceName($Record->id);
        }
        
        $ResourceArray = [];
        foreach($Resources as $Resource) {
            // remove Null resources
           if($Resource == null) {
               continue;
           } else {
               // add vaild resources
               $ResourceArray[] = $Resource;
           }
        }
        return $ResourceArray;
    }
    
    public static function AccountObject ($ApiToken) 
    {
        global $request;
        
        $Account = CommonHelper::GetAccountObject($ApiToken, null, null,null);
        if ($Account == null) {return null;}
        
        $TableName = CommonHelper::GetTableName($Account);

        $AccountResource = [];
        $AccountResource['apiToken']      = $Account->api_token;
        $AccountResource['phone']         = $Account->phone;
        $AccountResource['type']          = $TableName == 'users' ?  'user' : 'provider';
        // case users
        if ($TableName == 'users')     { $AccountResource['user'] = self::UserObject($Account->id);}
        // case providers
        if ($TableName == 'providers') { $AccountResource['provider'] = self::ProviderObject($Account->id);}

        return $AccountResource;
    }


    public static function CategoryObject ($id) 
    {
        global $request;
        $Category      = Category::find($id);
        if ($Category == null) {return null;}
        
        switch ($request->language) {
            case 'ar':  $CategoryName =  $Category->name_ar; break;
            case 'en':  $CategoryName =  $Category->name_en; break;
            default  :  $CategoryName =  $Category->name_ar; break;
        }
        $CategoryResource = [];
        $CategoryResource['id']                  = $Category->id;
        $CategoryResource['name']                = $CategoryName;
        $CategoryResource['image']               = $Category->image_url;
        $CategoryResource['link']                = $Category->link_url;

        return $CategoryResource;
    }

    public static function SliderObject ($id) 
    {
        global $request;
        $Slider      = Slider::find($id);
        if ($Slider == null) {return null;}
        
        switch ($request->language) {
            case 'ar':  $SliderImage =  $Slider->image_ar; break;
            case 'en':  $SliderImage =  $Slider->image_en; break;
            default  :  $SliderImage =  $Slider->image_ar; break;
        }

        $SliderResource = [];
        $SliderResource['id']                  = $Slider->id;
        $SliderResource['image']               = $SliderImage;
        $SliderResource['link']                = $Slider->link_url;

        return $SliderResource;
    }
    

    public static function BrandObject ($id) 
    {
        global $request;
        $Brand      = Brand::find($id);
        if ($Brand == null) {return null;}

        $BrandResource = [];
        $BrandResource['id']                  = $Brand->id;
        $BrandResource['image']               = $Brand->image_url;
        $BrandResource['link']                = $Brand->link_url;

        return $BrandResource;
    }

    public static function FeaturedObject ($id) 
    {
        global $request;
        $Featured      = Featured::find($id);
        if ($Featured == null) {return null;}

        switch ($request->language) {
            case 'ar':  $FeaturedImage =  $Featured->image_ar; break;
            case 'en':  $FeaturedImage =  $Featured->image_en; break;
            default  :  $FeaturedImage =  $Featured->image_ar; break;
        }

        $FeaturedResource = [];
        $FeaturedResource['id']                  = $Featured->id;
        $FeaturedResource['image']               = $FeaturedImage;
        $FeaturedResource['link']                = $Featured->link_url;

        return $FeaturedResource;
    }


    public static function UserObject ($id) 
    {
        $User = User::find($id);
        if ($User == null) {return null;}
 	
        $UserRecord = [];
        $UserRecord['id']                    = $User->id;
        $UserRecord['name']                  = $User->name;
        $UserRecord['balance']               = $User->balance;
        
        is_null($User->image) ?              : $UserRecord['image'] = $User->image;
        is_null($User->email) ?              : $UserRecord['email'] = $User->email;
        
        return $UserRecord;
    }
    
    public static function ProviderObject ($id) 
    {
        $Provider = Provider::find($id);
        if ($Provider == null) {return null;}
 	
        $ProviderRecord = [];
        $ProviderRecord['id']                    = $Provider->id;
        $ProviderRecord['name']                  = $Provider->name;
        $ProviderRecord['balance']               = $Provider->balance;
        
        is_null($Provider->beautyCenter) ?       : $ProviderRecord['beautyCenter']  = self::BeautyCenterObject($Provider->beautyCenter->id);
        
        return $ProviderRecord;
    }
    
    public static function AppInfoObject () 
    {
        global $request;
        $AppInfo = AppSetting::first();
        if ($AppInfo == null) {return null;}
        
        switch ($request->language) {
            case 'ar':  
                $AboutUs     =  $AppInfo->about_us_ar;
                $PolicyTerms =  $AppInfo->policy_term_ar;
            break;
            case 'en':  
                $AboutUs     =  $AppInfo->about_us_en;
                $PolicyTerms =  $AppInfo->policy_term_en;
            break;
            default:
                $AboutUs     =  $AppInfo->about_us_ar;
                $PolicyTerms =  $AppInfo->policy_term_ar;
            break;
        }
        
        $AppInfoRecord = [];
        $AppInfoRecord['phones']         = Phone::pluck('phone');
        $AppInfoRecord['emails']         = Email::pluck('email');
        $AppInfoRecord['aboutUs']        = $AboutUs;
        $AppInfoRecord['policyTerms']    = $PolicyTerms;
        
        return $AppInfoRecord;
    }
    
    public static function NotificationObject ($id) 
    {
        global $request;
        $NotifyUser = NotifyUser::find($id);
        if ($NotifyUser == null) {return null;}
        
        switch ($request->language) {
            case 'ar':  $Content =  $NotifyUser->Notification->content_ar; break;
            case 'en':  $Content =  $NotifyUser->Notification->content_en; break;
            default  :  $Content =  $NotifyUser->Notification->content_ar; break;
        }
        
        $NotifyUserRecord = [];
        $NotifyUserRecord['id']          = $id;
        $NotifyUserRecord['content']     = $Content;
        $NotifyUserRecord['isSeen']      = $NotifyUser->is_seen == 1 ? true : false;
        $NotifyUserRecord['time']        = strtotime($NotifyUser->created_at);

        is_null($NotifyUser->orders_id)     ? : $NotifyUserRecord['order'] = self::OrderObject($NotifyUser->orders_id);
        
        return $NotifyUserRecord;
    }
    
    public static function ReviewObject ($id) 
    {
        global $request;
        $Review = Review::find($id);
        if ($Review == null) {return null;}
        
        $ReviewRecord = [];
        $ReviewRecord['id']               = $Review->id;
        $ReviewRecord['rate']             = $Review->rate;
        $ReviewRecord['user']             = self::UserObject($Review->users_id);

        is_null($Review->comment)         ? : $ReviewRecord['comment'] = $Review->comment;
        return $ReviewRecord;
    }
    
    public static function BeautyCenterObject ($id) 
    {
        global $request;
        $BeautyCenter = BeautyCenter::find($id);
        if ($BeautyCenter == null) {return null;}

        $BeautyCenterRecord = [];
        $BeautyCenterRecord['id']               = $BeautyCenter->id;
        $BeautyCenterRecord['logo']             = $BeautyCenter->logo;
        $BeautyCenterRecord['name']             = $BeautyCenter->name;
        $BeautyCenterRecord['category']         = self::CategoryObject($BeautyCenter->categories_id);
        $BeautyCenterRecord['beautyCenterLevel']= self::LevelObject($BeautyCenter->beauty_center_level_id);
        $BeautyCenterRecord['branchNumbers']    = $BeautyCenter->branches_number;
        $BeautyCenterRecord['aboutBeautyCenter']= $BeautyCenter->about_beauty;
        $BeautyCenterRecord['gender']           = $BeautyCenter->gender;
        $BeautyCenterRecord['location']         = self::LocationObject($BeautyCenter->location->id);
        $BeautyCenterRecord['startWorkDay']     = $BeautyCenter->from_day;
        $BeautyCenterRecord['endWorkDay']       = $BeautyCenter->to_day;
        $BeautyCenterRecord['startWorkHour']    = $BeautyCenter->start_at;
        $BeautyCenterRecord['endWorkHour']      = $BeautyCenter->end_at;
        $BeautyCenterRecord['isFav']            = isset($request->apiToken) ? APIsHelper::IsFavouriteShop($request->apiToken, $id) : false;
        $BeautyCenterRecord['rate']             = $BeautyCenter->reviews()->avg('rate');
        $BeautyCenterRecord['provider']         = self::ProviderObject($BeautyCenter->providers_id);

        is_null($BeautyCenter->facebook_link)   ? : $BeautyCenterRecord['facebookLink']   = $BeautyCenter->facebook_link;
        is_null($BeautyCenter->twitter_link)    ? : $BeautyCenterRecord['twitterLink']    = $BeautyCenter->twitter_link;
        is_null($BeautyCenter->instagram_link)  ? : $BeautyCenterRecord['instagramLink']  = $BeautyCenter->instagram_link;
        count($BeautyCenter->images) == 0       ? : $BeautyCenterRecord['images']         = $BeautyCenter->images()->pluck('image');
        count($BeautyCenter->services) == 0     ? : $BeautyCenterRecord['services']       = self::ResourceArray('ServiceObject', $BeautyCenter->services);
        count($BeautyCenter->phones) == 0       ? : $BeautyCenterRecord['phones']         = $BeautyCenter->phones()->pluck('phone');
        return $BeautyCenterRecord;
    }
    
    public static function ServiceObject ($id) 
    {
        global $request;
        $Service = BeautyService::find($id);
        if ($Service == null) {return null;}

        $ServiceRecord = [];
        $ServiceRecord['id']      = $Service->id;
        $ServiceRecord['name']    = $Service->service;
        $ServiceRecord['price']   = $Service->price;
        return $ServiceRecord;
    }
    
    public static function OrderObject ($id) 
    {
        global $request;
        $Order = Order::find($id);
        if ($Order == null) {return null;}
        
        $BeautyCenter = BeautyCenter::where('providers_id', $Order->providers_id)->first();

        $OrderRecord = [];
        $OrderRecord['id']                  = $Order->id;
        $OrderRecord['services']            = self::ResourceArray('ServiceObject', BeautyService::find($Order->services()->pluck('beauty_services_id')->toArray()));
        $OrderRecord['mainLocation']        = self::LocationObject($Order->MainLocation->id);
        $OrderRecord['phone']               = $Order->phone;
        $OrderRecord['paymentMethod']       = $Order->payment_method;
        $OrderRecord['scheduleSession']     = strtotime($Order->schedule_session);
        $OrderRecord['total']               = $Order->total;
        $OrderRecord['status']              = $Order->status;
        $OrderRecord['user']                = self::UserObject($Order->users_id);
        $OrderRecord['beautyCenter']        = self::BeautyCenterObject($BeautyCenter->id);
        $OrderRecord['isDiscounted']        = APIsHelper::IsDiscountedOrder($Order->id);
        
        is_null($Order->cosmetic_material)       ? : $OrderRecord['cosmeticMaterial']   = $Order->cosmetic_material;
        is_null($Order->SecondaryLocation)       ? : $OrderRecord['secondaryLocation']  = self::LocationObject($Order->SecondaryLocation->id);
        is_null($Order->comment)                 ? : $OrderRecord['comment']            = $Order->extra_comment;
        return $OrderRecord;
    }
    

    
    public static function LevelObject ($id) 
    {
        global $request;
        $Level      = BeautyCenterLevel::find($id);
        if ($Level == null) {return null;}
        
        switch ($request->language) {
            case 'ar':  $LevelName =  $Level->level_ar; break;
            case 'en':  $LevelName =  $Level->level_en; break;
            default  :  $LevelName =  $Level->level_ar; break;
        }
        $LevelResource = [];
        $LevelResource['id']                  = $Level->id;
        $LevelResource['name']                = $LevelName;

        return $LevelResource;
    }
    
    public static function BankAccountObject ($id) 
    {
        global $request;
        $BankAccount      = BankAccount::find($id);
        if ($BankAccount == null) {return null;}
        
        switch ($request->language) {
            case 'ar':  $BankAccountName =  $BankAccount->name_ar; break;
            case 'en':  $BankAccountName =  $BankAccount->name_en; break;
            default  :  $BankAccountName =  $BankAccount->name_ar; break;
        }
        $BankAccountResource = [];
        $BankAccountResource['id']                  = $BankAccount->id;
        $BankAccountResource['image']               = $BankAccount->image;
        $BankAccountResource['bankName']            = $BankAccountName;
        $BankAccountResource['accountNumber']       = $BankAccount->account_number;
        $BankAccountResource['name']                = $BankAccount->person_name;
        $BankAccountResource['ipan']                = $BankAccount->ipan;

        return $BankAccountResource;
    }
    
    public static function AdObject ($id) 
    {
        $Ad      = BankAccount::find($id);
        if ($Ad == null) {return null;}
        
        $AdResource = [];
        $AdResource['id']                  = $Ad->id;
        $AdResource['image']               = $Ad->image;

        return $AdResource;
    }
    
    public static function LocationObject ($id) 
    {
        global $request;
        $Location = Location::find($id);
        if ($Location == null) {return null;}
        
        $LocationRecord = [];
        $LocationRecord['id']                = $Location->id;
        $LocationRecord['longitude']         = $Location->longitude;
        $LocationRecord['latitude']          = $Location->latitude;
        $LocationRecord['address']           = $Location->address;

        return $LocationRecord;
    }
}