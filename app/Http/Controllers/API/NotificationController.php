<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\CommonHelper;
use App\User;
use App\Models\Provider;
use App\Models\Notification;
use App\Models\NotifyUser;

class NotificationController extends Controller
{   
    public static function Notify ($NotificationId, $UserId, $ProviderId, $OrderId) 
    {
        if (isset($UserId)) {
            NotifyUser::CreateUpdate(['id' => null, 'notifications_id' => $NotificationId, 'users_id' => $UserId]);
             
            $Language = CommonHelper::GetAccountLanguage($UserId, null);
            $Message = self::GetMessage($NotificationId, $Language);
            self::FireBaseNotify($Message, $UserId, null);
        }
        
        if (isset($ProviderId)) {
            NotifyUser::CreateUpdate(['id' => null, 'notifications_id' => $NotificationId, 'providers_id' => $ProviderId]);
            $Language = CommonHelper::GetAccountLanguage(null, $ProviderId);
            $Message = self::GetMessage($NotificationId, $Language);
            self::FireBaseNotify($Message, null, $ProviderId);
        }
        
        // if (isset($OrderId)) {
        //     $Language = CommonHelper::GetAccountLanguage(null, $OrderId);
        //     $Message = self::GetMessage($NotificationId, $Language);
        //     self::FireBaseNotify($Message, null, $OrderId);
        // }
    }
    
    public static function GetMessage ($NotificationId, $Language) 
    {
        // return notification or null
        $Notification = Notification::find($NotificationId);
        switch ($Language) {
            case 'ar': $Message = $Notification->content_ar; break;
            case 'en': $Message = $Notification->content_en; break;
            default:   $Message = $Notification->content_ar; break;
        }
        return $Message;
    }
    
    public  static function FireBaseNotify ($Message, $UserId, $ProviderId) 
    {
        if ($UserId      != null)  { $Account    =  User::find($UserId);       }
        if ($ProviderId  != null)  { $Account    =  Provider::find($ProviderId);     }

        if ($Account !== null) {
            if ($Account->firebase_token !== null) {
                $handel =  self::SendFCM($Account->firebase_token, $Message);
            }
        } 
    }
    
    public static function SendFCM($FBToken, $Message) 
    {
        // push notification url
        $URL = 'https://fcm.googleapis.com/fcm/send';
        // 
        $Fields = array (
                'to' => $FBToken,
                'notification' => array (
                        "body" => $Message,
                        "title" => "306 App",
                        "icon" => "myicon"
                ),
                //'data' => array('type' => $AccountType, 'id' => $AccountId)
        );
        $Fields = json_encode ( $Fields );
        $Headers = array (
            'Authorization: key=' . "AIzaSyBeRmElaurFazBae9pJWuecqaNA-4dPrgc",
            'Content-Type: application/json'
        );
        // initilize curl
        $Curl = curl_init();
        curl_setopt ( $Curl, CURLOPT_URL, $URL );
        curl_setopt ( $Curl, CURLOPT_POST, true );
        curl_setopt ( $Curl, CURLOPT_HTTPHEADER, $Headers );
        curl_setopt ( $Curl, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $Curl, CURLOPT_POSTFIELDS, $Fields );
        // execute command
        $result = curl_exec ( $Curl );
        curl_close ( $Curl );
    }
    
}