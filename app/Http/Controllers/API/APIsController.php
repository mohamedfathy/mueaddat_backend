<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\API\CommonHelper;
use App\Http\Controllers\API\ResourceController;
use App\Http\Controllers\API\NotificationController;
use Illuminate\Support\Facades\DB;
use App\Models\Billing;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Option;
use App\Models\OptionGroup;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Payment;
use App\Models\Product;
use App\Models\ProductDetail;
use App\Models\ProductImage;
use App\Models\ProductOption;
use App\Models\ProductReview;
use App\Models\Shipping;
use App\Models\ShippingDetail;
use App\Models\Supplier;
use Hash;
use Carbon;
use App;

class APIsController extends Controller
{
    /**
     * index all main categories
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function getMainCategories (Request $request)
    {
        $rules = [
            "language"  => 'required|in:en,ar',
        ];

        $messages = [    
            "language.required" => 400,
            "language.in" =>  405,
        ];

        $Validation = CommonHelper::Validate($request->all(), $rules, $messages);
        if ($Validation !== null) { return $Validation; }

        $categories = DB::select('select * from categories where parent_id is null and is_active = 1');

        return response()->json(['status'=>200, 'categories' => ResourceController::ResourceArray('CategoryObject', $categories)]);
    } 

    /**
     * get the current application language
     * 
     * @return Illuminate\Http\Response
     */
    public function getLanguage ()
    {
        return response()->json(['status'=>200, 'language' => App::getLocale()]);
    }

    /**
     * get the sliders for home page
     * 
     * @return Illuminate\Http\Response
     */
    public function getSliders ()
    {
        $sliders = DB::select('select * from sliders where is_active = 1 order by created_at limit 3');

        return response()->json(['status' => 200, 'sliders' => ResourceController::ResourceArray('SliderObject', $sliders)]);
    }


    /**
     * get the brands for footer section
     * 
     * @return Illuminate\Http\Response
     */
    public function getBrands ()
    {
        $brands = DB::select('select * from brands where is_active = 1 order by created_at limit 18');

        return response()->json(['status' => 200, 'brands' => ResourceController::ResourceArray('BrandObject', $brands)]);
    }

    /**
     * get the sliders for home page
     * 
     * @return Illuminate\Http\Response
     */
    public function getFeatured ()
    {
        $featured = DB::select('select * from featured where is_active = 1 order by created_at limit 4');
        
        return response()->json(['status' => 200, 'featured' => ResourceController::ResourceArray('FeaturedObject', $featured)]);
    }
    


    public function registerProvider (Request $request)
    {
        $rules = [
            'name'		=> 'required|string|min:3|max:100',
            "email"     => 'nullable|email|unique:providers,email|unique:users,email|min:6', 
            "phone"     => 'required|regex:/^\+?\d[0-9-]{9,15}$/|unique:providers,phone|unique:users,phone',
            "password"  => 'required|string|min:6',
            "language"  => 'required|in:en,ar',
        ];

        $messages = [
            'name.required'=> 400,
            'name.string'  => 405,
            'name.min'     => 405,
            'name.max'     => 405,
    
            "email.email"  => 400,
            "email.unique" => 409,
            "email.min" => 405,
            
            "phone.required" => 400,
            "phone.regex" => 405,
            "phone.unique" => 408,
           
            "password.required" => 400,
            "password.string" => 405,
            "password.min" => 405,
    
            "language.required" => 400,
            "language.in" =>  405,
        ];

        $TextMessages = [
            "200" => ["en" => "successfully registered", "ar" => "تم التسجيل بنجاح"], 
            "408" => ["en" => "this phone registered before", "ar" => "هذا الهاتف مسجل مسبقا"], 
            "409" => ["en" => "this email registered before", "ar" => "هذا الايميل مسجل مسبقا"], 
        ];
        
        $Validation = CommonHelper::Validate($request->all(), $rules, $messages);
        if ($Validation !== null) { 
            // get Status Code
            $StatusCode = $Validation->getData()->status;
            return  ( $StatusCode ==  408 || $StatusCode ==  409 ) ?  response()->json(['status' => $StatusCode, 'message' => $TextMessages[$StatusCode][$request->language] ]) : $Validation;
        }

        // Create new record or update if id NOt equal Null
        $Account =  Provider::CreateUpdate(['id'=>null]);
        // Create random digits
        // $Digits = CommonHelper::RandomXDigits(6);
        $Digits = CommonHelper::$StaticXDigits;
        // save it in session
        Session::CreateUpdate(['id'=>null, 'phone'=>$request->phone, 'code'=>$Digits, 'providers_id'=>$Account->id]);

        // Send SMS Code
        // CommonHelper::SendSms($request->phone, $Digits);
        return response()->json(['status'=>200, 'account' => ResourceController::AccountObject($Account->api_token), 'message' => $TextMessages[200][$request->language] ]);

    } // end function
    
    public function validateCode (Request $request)
    {
        $rules = [
            "phone" => 'required_without_all:apiToken,tmpToken|regex:/^\+?\d[0-9-]{9,15}$/',
            "apiToken" => 'required_without_all:phone,tmpToken|string|min:69',
            "tmpToken" => 'required_without_all:phone,apiToken|string|min:69',
            "code" => 'required|string|min:6|max:6',
            "language"  => 'required|in:en,ar',
        ];

        $messages = [
            "phone.required_without_all" => 400,
            "phone.regex" => 405,
    
            "apiToken.required_without_all" => 400,
            "apiToken.string" => 405,
            "apiToken.min" => 405,
    
            "tmpToken.required_without_all" => 400,
            "tmpToken.string" => 405,
            "tmpToken.min" => 405,
            
            "code.required" => 400,
            "code.string" => 405,
            "code.min" => 405,
            "code.max" => 405,
            
            "language.required" => 400,
            "language.in" =>  405,
        ];
        
        $TextMessages = [
            "200" => ["en" => "successfully done", "ar" => "تمت العملية بنجاح"], 
            "410" => ["en" => "verification code is wrong or has expired", "ar" => "رمز التحقق خاطى او انتهت صلاحيته"], 
        ];
        
        $Validation = CommonHelper::Validate($request->all(), $rules, $messages);
        if ($Validation !== null) { return $Validation; }

        // First Case :: Register
        if (isset($request->phone)) { 
            if (CommonHelper::IsAccountActive(null, $request->phone, null, null) === null) { return response()->json(['status' => 402]);}  

            $Session = Session::where('tmp_phone', $request->phone)->where('code', $request->code)->first();
            
            if ($Session == null) {
                //  The request failed because wrong validate code.
                return response()->json(["status" => 410, 'message' => $TextMessages[410][$request->language]]);
            } else {
                if(CommonHelper::IsTimeOver($Session->created_at, 60) == true) {
                    // delete session
                    CommonHelper::DeleteSession($Session->id);
                    //exist but has pass hour 
                    return response()->json(["status" => 410, 'message' => $TextMessages[410][$request->language]]);
                }

                $Account = CommonHelper::GetAccountObject(null, $request->phone, null,null);
                if ($Account == null) {return response()->json(['status' => 403]);}
                // Mark User Verified
                $Account->is_verified = 1;
                $Account->save();
                // delete session
                CommonHelper::DeleteSession($Session->id);
                return response()->json(['status'=>200, 'apiToken' => $Account->api_token, 'message' => $TextMessages[200][$request->language] ]);
            } 

        }

        // Second Case :: Forget Password
        if (isset($request->tmpToken)) {
            $Session = Session::where('tmp_token', $request->tmpToken)->where('code', $request->code)->first();
            
            if ($Session == null) {
                return response()->json(["status" => 410,  'message' => $TextMessages[410][$request->language]]);
            } else {
                if(CommonHelper::IsTimeOver($Session->created_at, 60) == true) {
                    //exist but has pass hour 
                    return response()->json(["status" => 410, 'message' => $TextMessages[410][$request->language]]);
                }
                return response()->json(["status" => 200, 'message' => $TextMessages[200][$request->language]]);
            }    
        }
        
        // Third Case :: Update Phone
        if (isset($request->apiToken)) {

            $Account = CommonHelper::GetAccountObject($request->apiToken, null, null,null);
            if ($Account == null) {return response()->json(['status' => 403]);}
            
            $TableName = CommonHelper::GetTableName($Account);
            if($TableName == 'users') {
                 $Session = Session::where('users_id', $Account->id)->where('code', $request->code)->first();
            } else {
                 $Session = Session::where('providers_id', $Account->id)->where('code', $request->code)->first();
            }
           
            if ($Session == null) {
                return response()->json(["status" => 410]);
            } else {
                if(CommonHelper::IsTimeOver($Session->created_at, 60) == true) {
                    //exist but has pass hour 
                    return response()->json(["status" => 410]);
                }
                
                $Account->phone =$Session->tmp_phone;
                $Account->save();
                // delete session
                CommonHelper::DeleteSession($Session->id);
                return response()->json(['status'=>200]);
            }
        }
    } //end Function
    
    public function login (Request $request)
    {
        $rules = [
            // "type" => 'required|in:user,provider',
            "phone" => 'required|regex:/^\+?\d[0-9-]{9,15}$/',
            "password" => 'required|string|min:6', 
            "language" => 'required|in:ar,en',
        ];

        $messages = [
            // "type.required" => 400,
            // "type.in" => 405,
            
            "phone.required" => 400,
            "phone.regex" => 405,
        
            "password.required" => 400,
            "password.string" => 405,
            "password.min" => 405,
            
            "language.required" => 400,
            "language.in" =>  405,
        ];
        
        $TextMessages = [
            "200" => ["en" => "login successfully", "ar" => "تسجيل الدخول بنجاح"], 
            "414" => ["en" => "phone not verified", "ar" => "لم يتم التحقق من الهاتف"], 
            "415" => ["en" => "this phone is not registered", "ar" => "هذا الهاتف غير مسجل"], 
            "410" => ["en" => "password is wrong", "ar" => "كلمة المرور خاطئة"], 
        ];

        
        $Validation = CommonHelper::Validate($request->all(), $rules, $messages);
        if ($Validation !== null) { return $Validation; }

        // get Account Object
        $Account = CommonHelper::GetAccountObject(null, $request->phone, null, null);
        if ($Account == null) {return response()->json(['status' => 415, 'message' => $TextMessages[415][$request->language] ]);}
        
        if (CommonHelper::IsAccountValid(null, $request->phone, null, null) === null) { return response()->json(['status' => 414, 'message' => $TextMessages[414][$request->language] ]);} 

        if (Hash::check($request->password, $Account->password) == false) {
            return response()->json(['status' => 410 , 'message' => $TextMessages[410 ][$request->language] ]);
        } else {
            // generate new ApiToken
            CommonHelper::RegenerateAPIToken($Account);
            return response()->json(['status' => 200, 'account'=> ResourceController::AccountObject($Account->api_token), 'message' => $TextMessages[200][$request->language] ]);
        }
    } //end Function
    
    public function forgetPassword (Request $request)
    {
        $rules = [
            "phone" => 'required|regex:/^\+?\d[0-9-]{9,15}$/',
            "language" => 'required|in:ar,en',
        ];

        $messages = [
            "phone.required" => 400,
            "phone.regex" => 405,

            "language.required" => 400,
            "language.in" =>  405,
        ];
        
        $TextMessages = [
            "412" => ["en" => "this phone is not registered", "ar" => "هذا الهاتف غير مسجل"]
        ];

        $Validation = CommonHelper::Validate($request->all(), $rules, $messages);
        if ($Validation !== null) { return $Validation; }

        // get Account Object
        $Account = CommonHelper::GetAccountObject(null, $request->phone, null, null);
        if ($Account == null) {return response()->json(['status' => 412, 'message' => $TextMessages[412][$request->language] ]);}
        
        if (is_null(CommonHelper::IsAccountValid(null, $request->phone, null, null))) { return response()->json(['status' => 402]);} 
        
        $Digits = CommonHelper::$StaticXDigits;
        //$Digits = CommonHelper::RandomXDigits(6);
        $TempToken = CommonHelper::RandomXChars(69);
        // Send SMS Code
        // CommonHelper::SendSms($request->phone, $Digits);
        
        $TableName = CommonHelper::GetTableName($Account);
        if($TableName == 'users') {
            Session::CreateUpdate(['id'=>null, 'tmp_token'=>$TempToken, 'code'=>$Digits, 'users_id'=>$Account->id]);
        } else {
            Session::CreateUpdate(['id'=>null, 'tmp_token'=>$TempToken, 'code'=>$Digits, 'providers_id'=>$Account->id]);
        }
        
        return response()->json(['status' => 200, 'tmpToken' => $TempToken ]);
    } //end Function
    
    public function changePassword (Request $request)
    {
        $rules = [
            "tmpToken" => 'required|string|min:69|exists:sessions,tmp_token',
            "newPassword" => 'required|string|min:6',
        ];

        $messages = [
            "tmpToken.required" => 400,
            "tmpToken.string" => 405,
            "tmpToken.min" => 405,
            "tmpToken.exists" => 405,
            
            "newPassword.required" => 400,
            "newPassword.string" => 405,
            "newPassword.min" => 405,
        ];

        
        $Validation = CommonHelper::Validate($request->all(), $rules, $messages);
        if ($Validation !== null) { return $Validation; }

        // check if session exists
        $Session = CommonHelper::CheckSession($request->tmpToken, null, null,null);
        if ($Session == null) {return response()->json(['status' => 403]);}

        // if User
        if ($Session->users_id !== null) { 
            // get Account Object
            $Account = CommonHelper::GetAccountObjectById($Session->users_id, 'User');
            if($Account->is_active == 0) {return response()->json(['status' => 402]);}
        }
        // if Providers
        if ($Session->providers_id !== null) { 
            // get Account Object
            $Account = CommonHelper::GetAccountObjectById($Session->providers_id, 'Provider');
            if($Account->is_active == 0) {return response()->json(['status' => 402]);}
        }
        if ($Account == null) {return response()->json(['status' => 403]);}

        $Account->password = CommonHelper::HashPassword($request->newPassword);
        $Account->save();
        // Delete Session
        CommonHelper::DeleteSession($Session->id);
        
        return response()->json(['status' => 200]);
    } //end Function
    
    public function updatePassword (Request $request)
    {
        $rules = [
            "apiToken" => 'required|string|min:69',
            "oldPassword" => 'required|string|min:6',
            "newPassword" => 'required|string|min:6',
        ];

        $messages = [
            "apiToken.required" => 400,
            "apiToken.string" => 405,
            "apiToken.min" => 405,
    
            "oldPassword.required" => 400,
            "oldPassword.string" => 405,
            "oldPassword.min" => 405,
    
            "newPassword.required" => 400,
            "newPassword.string" => 405,
            "newPassword.min" => 405,
        ];

        
        $Validation = CommonHelper::Validate($request->all(), $rules, $messages);
        if ($Validation !== null) { return $Validation; }

        // get Account Object
        $Account = CommonHelper::GetAccountObject($request->apiToken, null, null,null);
        if ($Account == null) {return response()->json(['status' => 403]);}
        
        if (CommonHelper::IsAccountValid($request->apiToken, null, null, null) === null) { return response()->json(['status' => 402]);} 

        if (Hash::check($request->oldPassword, $Account->password) == false) { return CommonHelper::Response(411);}
        
        $Account->password = CommonHelper::HashPassword($request->newPassword);
        $Account->save();
        
        return response()->json(['status' => 200]);
    } //end Function
    
    public function resendCode (Request $request)
    {
        $rules = [
            "phone" => 'required|regex:/^\+?\d[0-9-]{9,15}$/',
            "language" => 'required|in:ar,en',
        ];

        $messages = [
            "phone.required" => 400,
            "phone.regex" => 405,
            
            "language.required" => 400,
            "language.in" =>  405,
        ];
        
        $TextMessages = [
            "200" => ["en" => "activation code sent", "ar" => "تم ارسال رمز التفعيل"], 
            "412" => ["en" => "failed. last code send less than 2 minutes ago", "ar" =>"فشل. اخر كود تم ارساله قبل اقل من دقيقتين."], 
        ];
        
        $Validation = CommonHelper::Validate($request->all(), $rules, $messages);
        if ($Validation !== null) { return $Validation; }
        
        // get Account Object
        $Account = CommonHelper::GetAccountObject(null, $request->phone, null,null);
        if ($Account == null) {return response()->json(['status' => 403]);}
        
        if (CommonHelper::IsAccountActive(null, $request->phone, null, null) === null) { return response()->json(['status' => 402]);} 

        $TableName = CommonHelper::GetTableName($Account);
        if ($TableName == 'users') {
            $Session = CommonHelper::CheckSessionById('User', $Account->id);
        } else {
            $Session = CommonHelper::CheckSessionById('Provider', $Account->id);
        }
        if ($Session == null) {return response()->json(['status' => 403]);}
        
        if(CommonHelper::IsTimeOver($Session->created_at, 2) == false) {
            // last resended code time is less than two minutes. 
            return response()->json(["status" => 412, 'message' => $TextMessages[412][$request->language]]);
        }
                
        // Create random digits
        $Digits = CommonHelper::RandomXDigits(6);
        // $Digits = CommonHelper::$StaticXDigits;
        // save it in session
        Session::CreateUpdate(['id'=>$Session->id, 'code'=>$Digits, 'resendCode' => true]);

        // Send SMS Code
        //CommonHelper::SendSms($request->phone, $Digits);
        return response()->json(['status'=>200, 'message' => $TextMessages[200][$request->language]]);
    } //end Function
    
    public function appInfo (Request $request)
    {
        $rules = [
            "language" => 'required|in:ar,en',
        ];

        $messages = [
            "language.required" => 400,
            "language.in" =>  405,
        ];

        $Validation = CommonHelper::Validate($request->all(), $rules, $messages);
        if ($Validation !== null) { return $Validation; }
        
        return response()->json(['status' => 200, 'appInfo' => ResourceController::AppInfoObject()]);
    } //end Function
    
    public function contact (Request $request)
    {
        $rules = [
            "apiToken" => 'required|string|min:69',
            "message" => 'required|string|min:3',
        ];

        $messages = [
            "apiToken.required" => 400,
            "apiToken.string" => 405,
            "apiToken.min" => 405,
            
            "message.required" => 400,
            "message.string" => 405,
            "message.min" => 405,
        ];
        
        $TextMessages = [
            "200" => ["en" => "sent successfully", "ar" => "تم الارسال بنجاح"]
        ];

        $Validation = CommonHelper::Validate($request->all(), $rules, $messages);
        if ($Validation !== null) { return $Validation; }
        
        // get Account Object
        $Account = CommonHelper::GetAccountObject($request->apiToken, null, null, null);
        if ($Account == null) {return response()->json(['status' => 403]);}
        if (CommonHelper::IsAccountActive($request->apiToken, null, null, null) === null) { return response()->json(['status' => 402]);} 
        
        $TableName = CommonHelper::GetTableName($Account);
        if($TableName == 'users') {Contact::CreateUpdate(['id'=>null, 'users_id' => $Account->id]);} 
        if($TableName == 'providers') {Contact::CreateUpdate(['id'=>null, 'providers_id' => $Account->id]);} 

        return response()->json(['status' => 200, 'message' => $TextMessages[200][$Account->language] ]);
    } //end Function
    
    public function unseenNotifications (Request $request)
    {
        $rules = [
            "apiToken" => 'required|string|min:69',
        ];

        $messages = [
            "apiToken.required" => 400,
            "apiToken.string" => 405,
            "apiToken.min" => 405,
        ];

        $Validation = CommonHelper::Validate($request->all(), $rules, $messages);
        if ($Validation !== null) { return $Validation; }
        
        if (CommonHelper::IsAccountActive($request->apiToken, null, null, null) === null) { return response()->json(['status' => 402]);} 

        // get Account Object
        $Account = CommonHelper::GetAccountObject($request->apiToken, null, null,null);
        if ($Account == null) {return response()->json(['status' => 403]);}

        $Count = 0;
        $TableName = CommonHelper::GetTableName($Account);
        if($TableName == 'users') { $Count = NotifyUser::where('users_id', $Account->id)->where('is_seen', 0)->count();} 
        if($TableName == 'providers') { $Count = NotifyUser::where('providers_id', $Account->id)->where('is_seen', 0)->count();}
            
        return response()->json(['status'=>200, 'unseen'=>$Count]);
    } //end Function
    
    public function notifications (Request $request)
    {
        $rules = [
            "apiToken" => 'required|string|min:69',
            "page" => 'required|int',
            "language" => 'required|in:ar,en',
        ];

        $messages = [
            "apiToken.required" => 400,
            "apiToken.string" => 405,
            "apiToken.min" => 405,
    
            "page.required" => 400,
            "page.int" => 405,
            
            "language.required" => 400,
            "language.in" => 405,
        ];

        $Validation = CommonHelper::Validate($request->all(), $rules, $messages);
        if ($Validation !== null) { return $Validation; }
        
        // get Account Object
        $Account = CommonHelper::GetAccountObject($request->apiToken, null, null, null);
        if ($Account == null) {return response()->json(['status' => 403]);}

        $TableName = CommonHelper::GetTableName($Account);
        if($TableName == 'users') {
            $Notifies = NotifyUser::where('users_id', $Account->id)
                                ->skip(CommonHelper::ResourcesToSkip($request->page))
                                ->take(CommonHelper::$PerPage)
                                ->orderBy('created_at', 'desc')
                                ->get();
        } else {
            $Notifies = NotifyUser::where('providers_id', $Account->id)
                                ->skip(CommonHelper::ResourcesToSkip($request->page))
                                ->take(CommonHelper::$PerPage)
                                ->orderBy('created_at', 'desc')
                                ->get();
        }
        CommonHelper::SetIsSeen($Notifies);
        if (CommonHelper::HasItems($Notifies) === false) {
            return CommonHelper::Response(204);
        } else {
            return response()->json(['status'=>200,'notifications' => ResourceController::ResourceArray('NotificationObject', $Notifies)]);
        }
    } //end Function
    
    public function favourite (Request $request)
    {
        $rules = [
            "apiToken" => 'required|string|min:69|exists:users,api_token',
            "beautyCenterId" => 'required|int|exists:beauty_centers,id'
        ];

        $messages = [
            "apiToken.required" => 400,
            "apiToken.string" => 405,
            "apiToken.min" => 405,
            "apiToken.exists" => 405,
    
            "beautyCenterId.required" => 400,
            "beautyCenterId.int" => 405,
            "beautyCenterId.exists" => 405,
        ];

        $Validation = CommonHelper::Validate($request->all(), $rules, $messages);
        if ($Validation !== null) { return $Validation; }
        
        if (CommonHelper::IsAccountActive($request->apiToken, null, null, null) === null) { return response()->json(['status' => 402]);} 

        $Account = CommonHelper::GetAccountObject($request->apiToken, null, null,null);
        if ($Account == null) {return response()->json(['status' => 401]);}
        
        $Favorite = Favorite::where('beauty_centers_id', $request->beautyCenterId)
                            ->where('users_id', $Account->id)
                            ->first();
        
        if ($Favorite == null) {
            Favorite::CreateUpdate(['id' => null, 'users_id' => $Account->id]);
            return response()->json(['status'=>200,'action'=>'favourite']);
        } else {
            $Favorite = Favorite::where('beauty_centers_id', $request->beautyCenterId)
                                ->where('users_id', $Account->id)
                                ->delete();
            return response()->json(['status'=>200,'action'=>'unfavourite']);
        }
    } //end Function
    
    public function getFavorite (Request $request)
    {
        $rules = [
            "apiToken" => 'required|string|min:69|exists:users,api_token',
            "page" => 'required|int',
        ];

        $messages = [
            "apiToken.required" => 400,
            "apiToken.string" => 405,
            "apiToken.min" => 405,
            "apiToken.exists" => 405,
    
            "page.required" => 400,
            "page.int" => 405,
        ];
        
        $Validation = CommonHelper::Validate($request->all(), $rules, $messages);
        if ($Validation !== null) { return $Validation; }

        if (CommonHelper::IsAccountActive($request->apiToken, null, null, null) === null) { return response()->json(['status' => 402]);} 
        
        $Account = CommonHelper::GetAccountObject($request->apiToken, null, null,null);
        if ($Account == null) {return response()->json(['status' => 401]);}

        $FavouriteBeautyCenterIds =  Favorite::where('users_id', $Account->id)->orderBy('created_at', 'desc')->pluck('beauty_centers_id')->toArray();
        $StringIds = implode(',', $FavouriteBeautyCenterIds);
        $BeautyCenters = BeautyCenter::whereIn('id', $FavouriteBeautyCenterIds)
                                    ->skip(CommonHelper::ResourcesToSkip($request->page))
                                    ->take(CommonHelper::$PerPage)
                                    ->orderByRaw(DB::raw("FIELD(id, $StringIds)"))
                                    ->get();
                        
        if (CommonHelper::HasItems($BeautyCenters) === false) {
            return CommonHelper::Response(204);
        } else {
            return response()->json(['status'=>200,'beautyCenters' => ResourceController::ResourceArray('BeautyCenterObject', $BeautyCenters)]);
        }
    } //end Function
    
    public function addReview (Request $request)
    {
        $rules = [
            "apiToken" => 'required|string|min:69|exists:users,api_token',
            "beautyCenterId" => 'required|int|exists:beauty_centers,id',
            "rate" => 'required|numeric',
            "comment" => 'nullable|string|min:3'
        ];

        $messages = [
            "apiToken.required" => 400,
            "apiToken.string" => 405,
            "apiToken.min" => 405,
            "apiToken.exists" => 405,
    
            "beautyCenterId.required" => 400,
            "beautyCenterId.int" => 405,
            "beautyCenterId.exists" => 405,
            
            "rate.required" => 400,
            "rate.numeric" => 405,
            
            "comment.string" => 400,
            "comment.min" => 405,
        ];
        
        $TextMessages = [
            "200" => ["en" => "your rating has been successfully added", "ar" => "تم اضافة تقييمك بنجاح"]
        ];
        
                
        $Validation = CommonHelper::Validate($request->all(), $rules, $messages);
        if ($Validation !== null) { return $Validation; }

        if (CommonHelper::IsAccountActive($request->apiToken, null, null, null) === null) { return response()->json(['status' => 402]);} 
        
        $Account = CommonHelper::GetAccountObject($request->apiToken, null, null,null);
        if ($Account == null) {return response()->json(['status' => 401]);}


        Review::CreateUpdate(['id' => null, 'users_id' => $Account->id ]);
        
        // Add Points to user Rate
        $User = User::find($Account->id);
        $AppSetting = AppSetting::first();
        $User->balance += $AppSetting->point_for_rating;
        
        //send notification to provider
        NotificationController::Notify(1, null, BeautyCenter::find($request->beautyCenterId)->providers_id, null);


        return response()->json(['status'=>200,'message' => $TextMessages[200][$Account->language] ]);
    } //end Function
    
    public function getReviews (Request $request)
    {
        $rules = [
            "apiToken" => 'required|string|min:69',
            "beautyCenterId" => 'required|int|exists:beauty_centers,id',
        ];

        $messages = [
            "apiToken.required" => 400,
            "apiToken.string" => 405,
            "apiToken.min" => 405,
    
            "beautyCenterId.required" => 400,
            "beautyCenterId.int" => 405,
            "beautyCenterId.exists" => 405
        ];
                
        $Validation = CommonHelper::Validate($request->all(), $rules, $messages);
        if ($Validation !== null) { return $Validation; }
        
        if (CommonHelper::IsAccountActive($request->apiToken, null, null, null) === null) { return response()->json(['status' => 402]);} 
        
        $Account = CommonHelper::GetAccountObject($request->apiToken, null, null,null);
        if ($Account == null) {return response()->json(['status' => 401]);}
        
        $Reviews = Review::where('beauty_center_id', $request->beautyCenterId)->get();    
        
        if (CommonHelper::HasItems($Reviews) === false) {
            return CommonHelper::Response(204);
        } else {
            return response()->json(['status'=>200, 'reviews' => ResourceController::ResourceArray('ReviewObject', $Reviews)]);
        }
    } //end Function
    
    public function addBeautyCenter (Request $request)
    {
        $rules = [
            "apiToken" => 'required|string|min:69|exists:providers,api_token',
            "logo" => 'required|string',
            "name" => 'required|string|min:3|max:100',
            "categoryId" => 'required|int|exists:categories,id',
            "beautyCenterLevelId" => 'required|int|exists:beauty_center_levels,id',
            "branchNumbers" => 'required|int|between:1,100',
            "aboutBeautyCenter" => 'required|string|min:3',
            "gender" => 'required|in:men,women,both',
            
            "location" => 'required',
            "location.longitude" => 'required|numeric',
            "location.latitude" => 'required|numeric',
            "location.address" => 'required|string',
            
            "startWorkDay" => 'required|string|min:3|max:45',
            "endWorkDay" => 'required|string|min:3|max:45',
            
            "startWorkHour" => 'required|date_format:H:i',
            "endWorkHour" => 'required|date_format:H:i',
            
            "phones" => 'nullable|array',
            "phones.*" => 'required|regex:/^\+?\d[0-9-]{9,15}$/|unique:beauty_phone,phone',
            
            "facebookLink" => 'nullable|url',
            "twitterLink" => 'nullable|url',
            "instagramLink" => 'nullable|url',
            
            "images" => 'nullable|array',
            "images.*" => 'required|string',
            
            "services" => 'nullable|array',
            "services.*.id" => 'nullable|int|exists:beauty_services,id',
            "services.*.name" => 'required|string|min:3|max:100',
            "services.*.price" => 'required|numeric',
        ];

        $messages = [
            "apiToken.required" => 400,
            "apiToken.string" => 405,
            "apiToken.min" => 405,
            "apiToken.exists" => 405,
    
            "logo.required" => 400,
            "logo.string" => 405,
            
            "name.required" => 400,
            "name.string" => 405,
            "name.min" => 405,
            "name.max" => 405,
            
            "categoryId.required" => 400,
            "categoryId.int" => 405,
            "categoryId.exists" => 405,
            
            "beautyCenterLevelId.required" => 400,
            "beautyCenterLevelId.int" => 405,
            "beautyCenterLevelId.exists" => 405,
            
            "branchNumbers.required" => 400,
            "branchNumbers.int" => 405,
            "branchNumbers.between" => 405,
            
            "aboutBeautyCenter.required" => 400,
            "aboutBeautyCenter.string" => 405,
            "aboutBeautyCenter.min" => 405,
            
            "gender.required" => 400,
            "gender.in" => 405,
            
            "location.required" => 400,
            
            "location.longitude.required" => 400,
            "location.longitude.numeric" => 405,
            
            "location.latitude.required" => 400,
            "location.latitude.numeric" => 405,
            
            "location.address.required" => 400,
            "location.address.string" => 405,
            
            "startWorkDay.required" => 400,
            "startWorkDay.string" => 405,
            "startWorkDay.min" => 405,
            "startWorkDay.max" => 405,
            
            "endWorkDay.required" => 400,
            "endWorkDay.string" => 405,
            "endWorkDay.min" => 405,
            "endWorkDay.max" => 405,
            
            "startWorkHour.required" => 400,
            "startWorkHour.date_format" => 405,
            
            "endWorkHour.required" => 400,
            "endWorkHour.date_format" => 405,
            
            "phones.array" => 405,
            
            "phones.required" => 400,
            "phones.regex" => 405,
            "phones.unique" => 405,
            
            "facebookLink.url" => 405,
            "twitterLink.url" => 405,
            "instagramLink.url" => 405,
            
            "images.array" => 405,
            
            "images.*.required" => 400,
            "images.*.string" => 405,
 
            "services.array" => 405,
            
            "services.*.id.int" => 400,
            "services.*.id.exists" => 405,
            
            "services.*.name.required" => 400,
            "services.*.name.string" => 405,
            "services.*.name.min" => 405,
            "services.*.name.max" => 405,
    
            "services.*.price.required" => 400,
            "services.*.price.numeric" => 405,
        ];
                
        $Validation = CommonHelper::Validate($request->all(), $rules, $messages);
        if ($Validation !== null) { return $Validation; }
        
        if (CommonHelper::IsAccountActive($request->apiToken, null, null, null) === null) { return response()->json(['status' => 402]);} 
        $Account = CommonHelper::GetAccountObject($request->apiToken, null, null,null);
        if ($Account == null) {return response()->json(['status' => 401]);}


        $BeautyCenter = BeautyCenter::CreateUpdate(['id' => null, 'providers_id' => $Account->id]);

        Location::CreateUpdate(['id' => null,
                                'longitude'=>$request->location['longitude'],
                                'latitude'=>$request->location['latitude'],
                                'address'=>$request->location['address'],
                                'beauty_center_id' => $BeautyCenter->id
                                ]);
        if(isset($request->phones))   { BeautyPhone::CreateUpdate(['id' => null, 'beauty_centers_id' =>$BeautyCenter->id]); }
        if(isset($request->images))   { BeautyImage::CreateUpdate(['id' => null, 'beauty_centers_id' =>$BeautyCenter->id]); }
        if(isset($request->services)) { BeautyService::CreateUpdate(['id' => null, 'beauty_centers_id' =>$BeautyCenter->id]); }
        
        return response()->json(['status'=>200, 'beautyCenter' => ResourceController::BeautyCenterObject($BeautyCenter->id)]);
    } //end Function
    
    public function getBeautyCenters (Request $request)
    {
        $rules = [
            "longitude" => 'required_with:latitude|required_if:nearest,1|numeric',
            "latitude" => 'required_with:longitude|required_if:nearest,1|numeric',
            
            "keyword" => 'nullable|string',
            
            "categories" => 'nullable|array',
            "categories.*" => 'required|int|exists:categories,id',
            
            "levels" => 'nullable|array',
            "levels.*" => 'required|int|exists:beauty_center_levels,id',
            
            "gender" => 'nullable|in:men,women,both',
            "nearest" => 'nullable|in:0,1',
            
            "highestRate" => 'nullable|boolean',
            "page" => 'required|int',
        ];

        $messages = [
            "longitude.required_with" => 400,
            "longitude.required_if" => 405,
            "longitude.numeric" => 405,
    
            "latitude.required_with" => 400,
            "latitude.required_if" => 405,
            "latitude.numeric" => 405,

            "keyword.string" => 405,

            "categories.array" => 405,
            "categories.*.required" => 405,
            "categories.*.int" => 405,
            "categories.*.exists" => 405,
            
            "levels.array" => 405,
            "levels.*.required" => 405,
            "levels.*.int" => 405,
            "levels.*.exists" => 405,
            
            "gender.in" => 405,
            "nearest.in" => 405,
            "highestRate.boolean" => 405,
            
            "page.required" => 400,
            "page.int" => 405
        ];

        $Validation = CommonHelper::Validate($request->all(), $rules, $messages);
        if ($Validation !== null) { return $Validation; }
  
        $query = BeautyCenter::query();
		$query->when($request->longitude, function ($q) {
			global $request;
            $BeautyCentersIds = Location::select(DB::raw('*, ( 6367 * acos( cos( radians('.$request->latitude.') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians('.$request->longitude.') ) + sin( radians('.$request->latitude.') ) * sin( radians( latitude ) ) ) ) AS distance'))
                                        ->having('distance', '<', 10)
                                        ->orderBy('distance')
                                        ->where('beauty_center_id', '!=', null)
                                        ->pluck('beauty_center_id')->toArray();
                                        
            $StringIds = implode(',', $BeautyCentersIds);
		    return $q->whereIn('id', $BeautyCentersIds)
		             ->orderByRaw(DB::raw("FIELD(id, $StringIds)"));
		});
		
		$query->when($request->keyword, function ($q) {
			global $request;
		    return $q->where('name', 'like', "%{$request->keyword}%")
		             ->orWhere('about_beauty', 'like', "%{$request->keyword}%");
		});
		
		$query->when($request->categories, function ($q) {
			global $request;
			return $q->whereIn('categories_id', $request->categories);
		});
		
		$query->when($request->levels, function ($q) {
			global $request;
			return $q->whereIn('beauty_center_level_id', $request->levels);
		});
		
		$query->when($request->gender, function ($q) {
			global $request;
			return $q->where('gender', $request->gender);
		});
		
		$query->when($request->highestRate, function ($q) {
			global $request;
			return $q->join('review', 'beauty_centers.id', '=', 'review.beauty_center_id')
			                 ->select('beauty_center_id',DB::raw('round(AVG(rate),0) as rate'))
                             ->groupBy('beauty_center_id')
                             ->orderBy('rate', 'desc');
		});
		
		
   		$BeautyCenter = $query->skip(CommonHelper::ResourcesToSkip($request->page))
                              ->take(CommonHelper::$PerPage)
                              ->where('is_active', 1)
                              ->get(); 

        if (CommonHelper::HasItems($BeautyCenter) === false) {
            return CommonHelper::Response(204);
        } else {
            return response()->json(['status'=>200,'beautyCenters' => ResourceController::ResourceArray('BeautyCenterObject', $BeautyCenter)]);
        }
    } //end Function
    
    public function makeOrder (Request $request)
    {
        $rules = [
            "apiToken" => 'required|string|min:69|exists:users,api_token',
            'beautyCenterId' => 'required|int|exists:beauty_centers,id',
            "services" => 'required|array',
            "services.*" => 'required|int|exists:beauty_services,id',
            "cosmeticMaterial" => 'nullable|string|min:3',
            "promoCode" => 'nullable|string',
            
            "mainLocation" => 'required',
            "mainLocation.longitude" => 'required|numeric',
            "mainLocation.latitude" => 'required|numeric',
            "mainLocation.address" => 'required|string',
            
            "secondaryLocation" => 'nullable',
            "secondaryLocation.longitude" => 'numeric',
            "secondaryLocation.latitude" => 'numeric',
            "secondaryLocation.address" => 'string',
            
            "phone"     => 'required|regex:/^\+?\d[0-9-]{9,15}$/',
            "paymentMethod"  => 'required|in:cash,visa,wallet',
            
            "scheduleSession"     => 'required|numeric',
            "comment"     => 'nullable|string',
        ];

        $messages = [
            "apiToken.required" => 400,
            "apiToken.string" => 405,
            "apiToken.min" => 405,
            "apiToken.exists" => 405,
            
            "beautyCenterId.required" => 400,
            "beautyCenterId.int" => 405,
            "beautyCenterId.exists" => 405,
            
            "services.required" => 400,
            "services.array" => 405,
            
            "services.*.required" => 400,
            "services.*.int" => 405,
            "services.*.exists" => 405,
            
            "cosmeticMaterial.string" => 405,
            "cosmeticMaterial.min" => 405,
            
            "promoCode.string" => 405,
                        
            "mainLocation.required" => 400,
            "mainLocation.longitude.required" => 400,
            "mainLocation.longitude.numeric" => 405,
            "mainLocation.latitude.required" => 400,
            "mainLocation.latitude.numeric" => 405,
            "mainLocation.address.required" => 400,
            "mainLocation.address.string" => 405,
            
            "secondaryLocation.longitude.numeric" => 405,
            "secondaryLocation.latitude.numeric" => 405,
            "secondaryLocation.address.string" => 405,
            
            "phone.required" => 400,
            "phone.regex" => 405,
    
            "paymentMethod.required" => 400,
            "paymentMethod.in" => 405,
            
            "scheduleSession.required" => 400,
            "scheduleSession.numeric" => 405,
            
            "comment.string" => 405,
        ];
        
        $TextMessages = [
            200 => ["en" => "order successfully submitted", "ar" => "تم ارسال الطلب بنجاح"], 
            413 => ["en"=> "order cannot be completed because code not correct", "ar" => "لا يمكن اكمال الطلب لان الكود غير صحيح"], 
            414 => ["en" => "order cannot be completed because code reached to limit", "ar" => "لا يمكن اكمال الطلب لان الرمز تم الوصول للحد المسموح للاستخدام"],
            415 => ["en" => "order cannot be completed because code used before", "ar" => "لايمكن اكمال الطلب لان الكود مستخدم من قبل"], 
            416 => ["en"=> "order cannot be completed because code not activated", "ar" => "لا يمكن اكمال الطلب بسبب عدم تفعيل الكود"], 
            417 => ["en" => "order cannot be completed because code ended", "ar" => "لا يمكن اكمال الطلب بسبب انتهاء الرمز"],
            418 => ["en" => "order cannot be completed because you do not have enough balance to paid order", "ar" => "لا يمكن اكمال الطلب لانك لا تملك رصيدا كافيا لدفع الطلب"],
        ];

        $Validation = CommonHelper::Validate($request->all(), $rules, $messages);
        if ($Validation !== null) { return $Validation; }
  
        if (CommonHelper::IsAccountActive($request->apiToken, null, null, null) === null) { return response()->json(['status' => 402]);} 
        
        $Account = CommonHelper::GetAccountObject($request->apiToken, null, null,null);
        if ($Account == null) {return response()->json(['status' => 401]);}
        
        // Checking for promo code
        if(isset($request->promoCode)) {
            $DiscountCode = DiscountCode::where('code', $request->promoCode)->first();
            if($DiscountCode == null) {return response()->json(['status'=>413,'message' => $TextMessages[413][$Account->language]]);}
            if($DiscountCode->count == 0) {return response()->json(['status'=>414,'message' => $TextMessages[414][$Account->language]]);}
            if($DiscountCode->is_active == 0) {return response()->json(['status'=>416,'message' => $TextMessages[416][$Account->language]]);}
            if(CommonHelper::GetDate() > $DiscountCode->end_date) {return response()->json(['status'=>417,'message' => $TextMessages[417][$Account->language]]);}
            $UsedCode = UsedCode::where('discount_code_id', $DiscountCode->id)->where('users_id', $Account->id)->first();
            if($UsedCode !== null) {return response()->json(['status'=>415,'message' => $TextMessages[415][$Account->language]]);}
        }
        
        $BeautyCenter = BeautyCenter::find($request->beautyCenterId);
        if($BeautyCenter == null) {return response()->json(['status'=>404]);}
        
        // getting App Setting
        $AppSetting = CommonHelper::GetAppSetting();
        if($AppSetting == null) {return response()->json(['status'=>404]);}
        $Total = 0;
        // Adding total price of services
        foreach ($request->services as $Service) { $Total = $Total + BeautyService::find($Service)->price;}

        // add Total Price to fees.
        $TotalWithFees = $Total + ($Total * ($AppSetting->fees / 100));
        // check user payment is wallet and  have enought balance.
        if ($request->paymentMethod == 'wallet') {if($TotalWithFees > $Account->balance) {return response()->json(['status'=>418 ,'message' => $TextMessages[418 ][$Account->language]]);}}
        
        // create orders
        $Order = Order::CreateUpdate(['id'=>null, 'fees' => $AppSetting->fees, 'total' => $TotalWithFees, 'users_id' => $Account->id, 'providers_id' => $BeautyCenter->providers_id ]);
        // create location 
        Location::CreateUpdate(['id' => null,'longitude'=>$request->mainLocation['longitude'],'latitude'=>$request->mainLocation['latitude'],'address'=>$request->mainLocation['address'],'order_main_id' => $Order->id]);
        if (isset($request->secondaryLocation)) {Location::CreateUpdate(['id' => null,'longitude'=>$request->secondaryLocation['longitude'],'latitude'=>$request->secondaryLocation['latitude'],'address'=>$request->secondaryLocation['address'],'order_secondary_id' => $Order->id]);}
        // create services
        foreach ($request->services as $Service) {
            $BeautyService = BeautyService::find($Service);
            OrderBeautyService::CreateUpdate(['id'=> null, 'orders_id' => $Order->id, 'beauty_services_id' => $BeautyService->id, 'price' => $BeautyService->price]);
        }

        if(isset($request->promoCode)) {
            // decrease the count of promo code by one.
            $DiscountCode->count -= 1; 
            $DiscountCode->save();
            // create record in db.
            UsedCode::CreateUpdate(['id' => null,'users_id'=>$Account->id, 'discount_code_id'=> $DiscountCode->id, 'orders_id' => $Order->id]);
            // discount from total with fees 
            $TotalWithFees = $TotalWithFees * ($DiscountCode->discount / 100);
            Order::CreateUpdate(['id'=>$Order->id, 'total' => $TotalWithFees ]);
        }
        
        //send notification to provider
        NotificationController::Notify(4, null, $BeautyCenter->providers_id, null);

        return response()->json(['status'=>200, 'message' => $TextMessages[200 ][$Account->language], 'order' => ResourceController::OrderObject($Order->id)]);

    } //end Function
    
    public function getOrders (Request $request)
    {
        $rules = [
            "apiToken" => 'required|string|min:69',
            "status" => 'nullable|in:waiting,accept,cancel',
            "page" => 'required|int',
        ];

        $messages = [
            "apiToken.required" => 400,
            "apiToken.string" => 405,
            "apiToken.min" => 405,
            
            "status.in" => 405,
                
            "page.required" => 400,
            "page.int" => 405,
        ];
        
        $Validation = CommonHelper::Validate($request->all(), $rules, $messages);
        if ($Validation !== null) { return $Validation; }
  
        if (CommonHelper::IsAccountActive($request->apiToken, null, null, null) === null) { return response()->json(['status' => 402]);} 
        
        $Account = CommonHelper::GetAccountObject($request->apiToken, null, null,null);
        if ($Account == null) {return response()->json(['status' => 401]);}
        
        $TableName = CommonHelper::GetTableName($Account);
        $Key = $TableName == 'users' ? 'users_id' : 'providers_id';
        
        if(isset($request->status)) {
            $Order = Order::where("{$Key}", $Account->id)->where('status', $request->status)
                            ->skip(CommonHelper::ResourcesToSkip($request->page))
                            ->take(CommonHelper::$PerPage)->orderBy('created_at', 'desc')
                            ->get();
        } else {
            $Order = Order::where("{$Key}", $Account->id)
                            ->skip(CommonHelper::ResourcesToSkip($request->page))
                            ->take(CommonHelper::$PerPage)->orderBy('created_at', 'desc')
                            ->get();
        }
            
        if (CommonHelper::HasItems($Order) === false) {
            return CommonHelper::Response(204);
        } else {
            return response()->json(['status'=>200,'orders' => ResourceController::ResourceArray('OrderObject', $Order)]);
        }
    } //end Function
    
    public function changeLanguage (Request $request)
    {
        $rules = [
            "apiToken" => 'required|string|min:69',
            "language"  => 'required|in:en,ar'
        ];

        $messages = [
            "apiToken.required" => 400,
            "apiToken.string" => 405,
            "apiToken.min" => 405,
    
            "language.required" => 400,
            "language.in" =>  405,
        ];
        
        $Validation = CommonHelper::Validate($request->all(), $rules, $messages);
        if ($Validation !== null) { return $Validation; }
        
        if (CommonHelper::IsAccountActive($request->apiToken, null, null, null) === null) { return response()->json(['status' => 402]);} 
        
        $Account = CommonHelper::GetAccountObject($request->apiToken, null, null,null);
        if ($Account == null) {return response()->json(['status' => 401]);}
        
        $Account->language = $request->language;
        $Account->save();
        
        return response()->json(['status'=>200]);
    } //end Function
    
    public function setFireBaseToken (Request $request)
    {
        $rules = [
            "apiToken" => 'required|string|min:69',
            "fireToken"  => 'required|string'
        ];

        $messages = [
            "apiToken.required" => 400,
            "apiToken.string" => 405,
            "apiToken.min" => 405,
    
            "fireToken.required" => 400,
            "fireToken.string" =>  405,
        ];
        
        $Validation = CommonHelper::Validate($request->all(), $rules, $messages);
        if ($Validation !== null) { return $Validation; }
        
        if (CommonHelper::IsAccountActive($request->apiToken, null, null, null) === null) { return response()->json(['status' => 402]);} 
        
        $Account = CommonHelper::GetAccountObject($request->apiToken, null, null,null);
        if ($Account == null) {return response()->json(['status' => 401]);}
        
        $Account->firebase_token = $request->fireToken;
        $Account->save();
        
        return response()->json(['status'=>200]);
    } //end Function
    
    public function logout (Request $request)
    {
        $rules = [
            "apiToken" => 'required|string|min:69',
        ];

        $messages = [
            "apiToken.required" => 400,
            "apiToken.string" => 405,
            "apiToken.min" => 405,
        ];
        
        $Validation = CommonHelper::Validate($request->all(), $rules, $messages);
        if ($Validation !== null) { return $Validation; }
        
        if (CommonHelper::IsAccountActive($request->apiToken, null, null, null) === null) { return response()->json(['status' => 402]);} 
        
        $Account = CommonHelper::GetAccountObject($request->apiToken, null, null,null);
        if ($Account == null) {return response()->json(['status' => 401]);}
        
        $Account->firebase_token = null;
        $Account->save();
        
        return response()->json(['status'=>200]);
    } //end Function
    
    public function acceptOrder (Request $request)
    {
        $rules = [
            "apiToken" => 'required|string|min:69|exists:providers,api_token',
            "orderId" => 'required|int|exists:orders,id',
        ];

        $messages = [
            "apiToken.required" => 400,
            "apiToken.string" => 405,
            "apiToken.min" => 405,
            "apiToken.exists" => 405,
            
            "orderId.required" => 400,
            "orderId.int" => 405,
            "orderId.exists" => 405,
        ];
        
        $Validation = CommonHelper::Validate($request->all(), $rules, $messages);
        if ($Validation !== null) { return $Validation; }
        
        if (CommonHelper::IsAccountActive($request->apiToken, null, null, null) === null) { return response()->json(['status' => 402]);} 
        
        $Account = CommonHelper::GetAccountObject($request->apiToken, null, null,null);
        if ($Account == null) {return response()->json(['status' => 401]);}
        
        // Accept user order
        $Order = Order::find($request->orderId);
        if($Order->providers_id !== $Account->id) { return response()->json(['status'=>403]);}
        if($Order->status !== 'waiting') { return response()->json(['status'=>440]);}
        // check provider have enougth balance and discount balance
        $Provider = Provider::find($Order->providers_id);
        $AppSetting = CommonHelper::GetAppSetting();
        $ProvidorCutOffs = $Order->total * ($AppSetting->fees / 100);
        if($ProvidorCutOffs > $Provider->balance) {return response()->json(['status'=>430]);}
        // $Provider->balance -= $ProvidorCutOffs;
        // $Provider->save();
        
        // Add points to user Rewarding him
        $User = User::find($Order->users_id);
        $User->balance += $AppSetting->point_for_new_order;
        $User->save();
        
        // discout from user balance and add to provider balance if wallet
        if ($Order->payment_method == "wallet") {
            $NetCost = $Order->total - ($Order->total * ($Order->fees / 100));
            if (CommonHelper::CheckForBalance($User->id, $NetCost)) {
                // take balance to User account
                $User->balance -= $NetCost;
                $User->save();
                // add balance to Provider account
                $Provider = Provider::find($Order->providers_id);
                $Provider->balance += $NetCost - $ProvidorCutOffs ;
                $Provider->save();
            } else {
                // there is not enought balance to do transaction
                return response()->json(['status'=>420]);
            }
        }
        
        // accept order
        $Order->status = 'accept';
        $Order->save();
        
        //send notification to provider
        NotificationController::Notify(3, $Order->users_id, null, null);
            
        return response()->json(['status'=>200]);
    } //end Function
    
    public function cancelOrder (Request $request)
    {
        $rules = [
            "apiToken" => 'required|string|min:69|exists:providers,api_token',
            "orderId" => 'required|int|exists:orders,id',
        ];

        $messages = [
            "apiToken.required" => 400,
            "apiToken.string" => 405,
            "apiToken.min" => 405,
            "apiToken.exists" => 405,
            
            "orderId.required" => 400,
            "orderId.int" => 405,
            "orderId.exists" => 405,
        ];
        
        $Validation = CommonHelper::Validate($request->all(), $rules, $messages);
        if ($Validation !== null) { return $Validation; }
        
        if (CommonHelper::IsAccountActive($request->apiToken, null, null, null) === null) { return response()->json(['status' => 402]);} 
        
        $Account = CommonHelper::GetAccountObject($request->apiToken, null, null,null);
        if ($Account == null) {return response()->json(['status' => 401]);}
        
        $Order = Order::find($request->orderId);
        
        if($Order->providers_id !== $Account->id) { return response()->json(['status'=>403]);}
        if($Order->status !== 'waiting') { return response()->json(['status'=>440]);}
        $Order->status = 'cancel';
        $Order->save();
        
        //send notification to provider
        NotificationController::Notify(2, $Order->users_id, null, null);
            
        return response()->json(['status'=>200]);
    } //end Function
    
    public function editBeautyCenter (Request $request)
    {
        $rules = [
            "apiToken" => 'required|string|min:69|exists:providers,api_token',
            "beautyCenterId" => 'required|int|exists:beauty_centers,id',
            "logo" => 'nullable|string',
            "name" => 'nullable|string|min:3|max:100',
            "categoryId" => 'nullable|int|exists:categories,id',
            "beautyCenterLevelId" => 'nullable|int|exists:beauty_center_levels,id',
            "branchNumbers" => 'nullable|int|between:1,100',
            "aboutBeautyCenter" => 'nullable|string|min:3',
            "gender" => 'nullable|in:men,women,both',
            
            "location" => 'nullable',
            "location.longitude" => 'numeric',
            "location.latitude" => 'numeric',
            "location.address" => 'string',
            
            "startWorkDay" => 'nullable|string|min:3|max:45',
            
            "endWorkDay" => 'nullable|string|min:3|max:45',
            
            "startWorkHour" => 'nullable|date_format:H:i',
            "endWorkHour" => 'nullable|date_format:H:i',
            
            "phones" => 'nullable|array',
            "phones.*" => 'required|regex:/^\+?\d[0-9-]{9,15}$/|unique:beauty_phone,phone',
            
            "facebookLink" => 'nullable|url',
            "twitterLink" => 'nullable|url',
            "instagramLink" => 'nullable|url',
            
            "images" => 'nullable|array',
            "images.*" => 'required|string',
            
            "services" => 'nullable|array',
            "services.*.id" => 'nullable|int|exists:beauty_services,id',
            "services.*.name" => 'required|string|min:3|max:100',
            "services.*.price" => 'required|numeric',
        ];

        $messages = [
            "apiToken.required" => 400,
            "apiToken.string" => 405,
            "apiToken.min" => 405,
            "apiToken.exists" => 405,
            
            "beautyCenterId.required" => 400,
            "beautyCenterId.int" => 405,
            "beautyCenterId.exists" => 405,
    
            "logo.string" => 405,
            
            "name.string" => 405,
            "name.min" => 405,
            "name.max" => 405,
            
            "categoryId.int" => 405,
            "categoryId.exists" => 405,
            
            "beautyCenterLevelId.int" => 405,
            "beautyCenterLevelId.exists" => 405,
            
            "branchNumbers.int" => 405,
            "branchNumbers.between" => 405,
            
            "aboutBeautyCenter.string" => 405,
            "aboutBeautyCenter.min" => 405,
            
            "gender.in" => 405,
            
            
            //"location.longitude.required" => 400,
            "location.longitude.numeric" => 405,
            
            //"location.latitude.required" => 400,
            "location.latitude.numeric" => 405,
            
            //"location.address.required" => 400,
            "location.address.string" => 405,
            
            "startWorkDay.string" => 405,
            "startWorkDay.date_format" => 405,
            
            "endWorkDay.string" => 405,
            "endWorkDay.date_format" => 405,
            
            "startWorkHour.between" => 405,
            
            "endWorkHour.between" => 405,
            
            "phones.array" => 405,
            
            "phones.required" => 400,
            "phones.regex" => 405,
            "phones.unique" => 405,
            
            "facebookLink.url" => 405,
            "twitterLink.url" => 405,
            "instagramLink.url" => 405,
            
            "images.array" => 405,
            
            "images.required" => 400,
            "images.string" => 405,
 
            "services.array" => 405,
            
            "services.*.id.int" => 400,
            "services.*.id.exists" => 405,
            
            "services.*.name.required" => 400,
            "services.*.name.string" => 405,
            "services.*.name.min" => 405,
            "services.*.name.max" => 405,
    
            "services.*.price.required" => 400,
            "services.*.price.numeric" => 405,
        ];

        
        $Validation = CommonHelper::Validate($request->all(), $rules, $messages);
        if ($Validation !== null) { return $Validation; }
        
        if (CommonHelper::IsAccountActive($request->apiToken, null, null, null) === null) { return response()->json(['status' => 402]);} 
        
        $Account = CommonHelper::GetAccountObject($request->apiToken, null, null,null);
        if ($Account == null) {return response()->json(['status' => 401]);}
        
        // check user is authorized to edit
        $BCenter = BeautyCenter::find($request->beautyCenterId);
        if($BCenter->providers_id !== $Account->id) { return response()->json(['status'=>403]);}

        $BeautyCenter = BeautyCenter::CreateUpdate(['id' => $request->beautyCenterId]);
        if(isset($request->location)) { $BeautyCenter->location()->delete(); Location::CreateUpdate(['id' => null,'longitude'=>$request->location['longitude'],'latitude'=>$request->location['latitude'],'address'=>$request->location['address'],'beauty_center_id' => $BeautyCenter->id]);}
        if(isset($request->phones))   { $BeautyCenter->phones()->delete(); BeautyPhone::CreateUpdate(['id' => null, 'beauty_centers_id' =>$BeautyCenter->id]); }
        if(isset($request->images))   { $BeautyCenter->images()->delete(); BeautyImage::CreateUpdate(['id' => null, 'beauty_centers_id' =>$BeautyCenter->id]); }
        if(isset($request->services)) { $BeautyCenter->services()->delete(); BeautyService::CreateUpdate(['id' => null, 'beauty_centers_id' =>$BeautyCenter->id]); }

        return response()->json(['status'=>200, 'beautyCenter' => ResourceController::BeautyCenterObject($BeautyCenter->id)]);
    } //end Function
    
    public function getCategories (Request $request)
    {
        $rules = [
            "language" => 'required|in:ar,en',
        ];

        $messages = [
            "language.required" => 400,
            "language.in" => 405,
        ];
        
                
        $Validation = CommonHelper::Validate($request->all(), $rules, $messages);
        if ($Validation !== null) { return $Validation; }
        
        $Categories = Category::where('is_active', 1)->get();
        
        if (CommonHelper::HasItems($Categories) === false) {
            return CommonHelper::Response(204);
        } else {
            return response()->json(['status'=>200,'categories' => ResourceController::ResourceArray('CategoryObject', $Categories)]);
        }
    } //end Function
    
    public function getLevels (Request $request)
    {
        $rules = [
            "language" => 'required|in:ar,en',
        ];

        $messages = [
            "language.required" => 400,
            "language.in" => 405,
        ];
                
        $Validation = CommonHelper::Validate($request->all(), $rules, $messages);
        if ($Validation !== null) { return $Validation; }
        
        $levels = BeautyCenterLevel::where('is_active', 1)->get();
        
        if (CommonHelper::HasItems($levels) === false) {
            return CommonHelper::Response(204);
        } else {
            return response()->json(['status'=>200,'levels' => ResourceController::ResourceArray('LevelObject', $levels)]);
        }
    } //end Function
    
    public function getbankAccounts (Request $request)
    {
        $rules = [
            "apiToken" => 'required|string|min:69',
        ];

        $messages = [
            "apiToken.required" => 400,
            "apiToken.string" => 405,
            "apiToken.min" => 405,
        ];
                
        $Validation = CommonHelper::Validate($request->all(), $rules, $messages);
        if ($Validation !== null) { return $Validation; }
                
        if (CommonHelper::IsAccountActive($request->apiToken, null, null, null) === null) { return response()->json(['status' => 402]);} 
        
        $Account = CommonHelper::GetAccountObject($request->apiToken, null, null,null);
        if ($Account == null) {return response()->json(['status' => 401]);}
        
        $BankAccounts = BankAccount::where('is_active', 1)->get();
        
        if (CommonHelper::HasItems($BankAccounts) === false) {
            return CommonHelper::Response(204);
        } else {
            return response()->json(['status'=>200,'bankAccounts' => ResourceController::ResourceArray('BankAccountObject', $BankAccounts)]);
        }
    } //end Function
    
    public function rechargeBalance (Request $request)
    {
        $rules = [
            "apiToken" => 'required|string|min:69',
            "image" => 'required|string',
        ];

        $messages = [
            "apiToken.required" => 400,
            "apiToken.string" => 405,
            "apiToken.min" => 405,
            
            "image.required" => 400,
            "image.string" => 405,
        ];
        
        
        $Validation = CommonHelper::Validate($request->all(), $rules, $messages);
        if ($Validation !== null) { return $Validation; }
        
        if (CommonHelper::IsAccountActive($request->apiToken, null, null, null) === null) { return response()->json(['status' => 402]);} 
        
        $Account = CommonHelper::GetAccountObject($request->apiToken, null, null,null);
        if ($Account == null) {return response()->json(['status' => 401]);}
        
        $TableName = CommonHelper::GetTableName($Account);
        if($TableName == 'users') {
            BalanceRecharge::CreateUpdate(['id' => null, 'users_id' => $Account->id]);
        } else {
            BalanceRecharge::CreateUpdate(['id' => null, 'providers_id' => $Account->id]);
        }
        return response()->json(['status'=>200]);
    } //end Function
    
    public function ads (Request $request)
    {
        $Ads = Ad::where('is_active', 1)
                ->where('end_date', '>=', CommonHelper::GetDate())
                ->get();
        if (CommonHelper::HasItems($Ads) === false) {
            return CommonHelper::Response(204);
        } else {
            return response()->json(['status'=>200,'ads' => ResourceController::ResourceArray('AdObject', $Ads)]);
        }
    } //end Function
    
    
    public function updateUserProfile (Request $request)
    {
        $rules = [
            "apiToken"  => 'required|string|min:69|exists:users,api_token',
            'name'		=> 'nullable|string|min:3|max:100',
            "image"     => 'nullable|string',
            "email"     => 'nullable|email|unique:users,email|unique:providers,email|min:6', 
            "newPhone"     => 'nullable|regex:/^\+?\d[0-9-]{9,15}$/|unique:users,phone|unique:providers,phone',
        ];

        $messages = [
            "apiToken.required" => 400,
            "apiToken.string" => 405,
            "apiToken.min" => 405,
            "apiToken.exists" => 405,
        
            'name.string'  => 405,
            'name.min'     => 405,
            'name.max'     => 405,
    
            "image.string" => 405,  
            
            "email.email"  => 400,
            "email.unique" => 407,
            "email.min" => 405,
            
            "newPhone.regex" => 405,
            "newPhone.unique" => 406,
        ];

        $TextMessages = [
            200 => ["en" => "successfully updated", "ar" => "تم تحديث البروفايل  بنجاح"], 
            406 => ["en" => "this phone registered before", "ar" => "هذا الهاتف مسجل مسبقا"], 
            407 => ["en" => "this email registered before", "ar" => "هذا الايميل مسجل مسبقا"]
        ];
        
        $Validation = CommonHelper::Validate($request->all(), $rules, $messages);
        if ($Validation !== null) { 
            // get Status Code
            $StatusCode = $Validation->getData()->status;
            return  ( $StatusCode ==  406 || $StatusCode ==  407 ) ?  response()->json(['status' => $StatusCode, 'message' => $TextMessages[$StatusCode][$request->language] ]) : $Validation;
        }
        
        if (CommonHelper::IsAccountActive($request->apiToken, null, null, null) === null) { return response()->json(['status' => 402]);} 
        $Account = CommonHelper::GetAccountObject($request->apiToken, null, null,null);
        if ($Account == null) {return response()->json(['status' => 401]);}
        
        // Create random digits
        //$Digits = CommonHelper::RandomXDigits(6);
        $Digits = CommonHelper::$StaticXDigits;
        !isset($request->newPhone) ? :  Session::CreateUpdate(['id'=>null, 'phone' =>$request->newPhone, 'code'=>$Digits, 'users_id'=>$Account->id]);

        $Account = isset($request->image) ? User::CreateUpdate(['id'=>$Account->id, 'image' => CommonHelper::BaseSixtyFourImage($request->image, 'user')]) : User::CreateUpdate(['id'=>$Account->id]);
        
        return response()->json(['status' => 200, 'account' => ResourceController::AccountObject($Account->api_token), 'message' => $TextMessages[200][$request->language]]);

    } //end Function 
    
    public function updateProviderProfile (Request $request)
    {
        $rules = [
            "apiToken"  => 'required|string|min:69|exists:providers,api_token',
            'name'		=> 'nullable|string|min:3|max:100',
            "email"     => 'nullable|email|unique:users,email|unique:providers,email|min:6', 
            "phone"     => 'nullable|regex:/^\+?\d[0-9-]{9,15}$/|unique:users,phone|unique:providers,phone',
        ];

        $messages = [
            "apiToken.required" => 400,
            "apiToken.string" => 405,
            "apiToken.min" => 405,
            "apiToken.exists" => 405,
        
            'name.string'  => 405,
            'name.min'     => 405,
            'name.max'     => 405,
            
            "email.email"  => 400,
            "email.unique" => 407,
            "email.min" => 405,
            
            "phone.regex" => 405,
            "phone.unique" => 406,
        ];

        $TextMessages = [
            200 => ["en" => "successfully registered", "ar" => "تم التسجيل بنجاح"], 
            406 => ["en" => "this phone registered before", "ar" => "هذا الهاتف مسجل مسبقا"], 
            407 => ["en" => "this email registered before", "ar" => "هذا الايميل مسجل مسبقا"]
        ];
        
        $Validation = CommonHelper::Validate($request->all(), $rules, $messages);
        if ($Validation !== null) { 
            // get Status Code
            $StatusCode = $Validation->getData()->status;
            return  ( $StatusCode ==  406 || $StatusCode ==  407 ) ?  response()->json(['status' => $StatusCode, 'message' => $TextMessages[$StatusCode][$request->language] ]) : $Validation;
        }
        
        if (CommonHelper::IsAccountActive($request->apiToken, null, null, null) === null) { return response()->json(['status' => 402]);} 
        $Account = CommonHelper::GetAccountObject($request->apiToken, null, null,null);
        if ($Account == null) {return response()->json(['status' => 401]);}
        
        // Create random digits
        //$Digits = CommonHelper::RandomXDigits(6);
        $Digits = CommonHelper::$StaticXDigits;
        !isset($request->newPhone) ? :  Session::CreateUpdate(['id'=>null, 'phone' =>$request->newPhone, 'code'=>$Digits, 'providers_id'=>$Account->id]);

        $Account = Provider::CreateUpdate(['id'=>$Account->id]);
        
        return response()->json(['status' => 200, 'account' => ResourceController::AccountObject($Account->api_token), 'message' => $TextMessages[200][$request->language]]);

    } //end Function 
}