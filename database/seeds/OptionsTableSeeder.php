<?php

use Illuminate\Database\Seeder;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $options = [
            [
                'option_name_ar' => '7 بوصة',
                'option_name_en' => '7 inches',
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],
            [

                'option_name_ar' => 'Api2i',
                'option_name_en' => 'Api2i',
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],
            [ 
                'option_name_ar' => '1200 واط',
                'option_name_en' => '1200 watt',
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],

        ];


        DB::table('options')->insert($options);
    }
}
