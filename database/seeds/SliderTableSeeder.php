<?php

use Illuminate\Database\Seeder;

class SliderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sliders = [
            [
                'id' => 1,  
                'image_ar' => 'uploads/sliders/one.jpg',
                'image_en' => 'uploads/sliders/one.jpg',
                'link_url' => 'hand_tools',
                'is_active' => 1,
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],
            [
                'id' => 2,  
                'image_ar' => 'uploads/sliders/two.jpg',
                'image_en' => 'uploads/sliders/two.jpg',
                'link_url' => 'hand_tools',
                'is_active' => 1,
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],
            [
                'id' => 3,  
                'image_ar' => 'uploads/sliders/three.jpg',
                'image_en' => 'uploads/sliders/three.jpg',
                'link_url' => 'hand_tools',
                'is_active' => 1,
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],

        ];


        DB::table('sliders')->insert($sliders);
    }
}
