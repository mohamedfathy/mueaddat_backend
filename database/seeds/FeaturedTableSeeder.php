<?php

use Illuminate\Database\Seeder;

class FeaturedTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $featured = [
            [
                'id' => 1,  
                'image_ar' => 'uploads/featured/one.jpg',
                'image_en' => 'uploads/featured/one.jpg',
                'link_url' => 'hand_tools',
                'is_active' => 1,
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],
            [
                'id' => 2,  
                'image_ar' => 'uploads/featured/two.jpg',
                'image_en' => 'uploads/featured/two.jpg',
                'link_url' => 'power_tools',
                'is_active' => 1,
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],
            [
                'id' => 3,  
                'image_ar' => 'uploads/featured/three.jpg',
                'image_en' => 'uploads/featured/three.jpg',
                'link_url' => 'air_tools',
                'is_active' => 1,
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],
            [
                'id' => 4,  
                'image_ar' => 'uploads/featured/four.jpg',
                'image_en' => 'uploads/featured/four.jpg',
                'link_url' => 'accessories',
                'is_active' => 1,
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],

        ];
        
        DB::table('featured')->insert($featured);
    }
}
