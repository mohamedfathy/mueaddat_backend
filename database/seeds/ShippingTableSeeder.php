<?php

use Illuminate\Database\Seeder;

class ShippingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $shipping = [
            [
                'id' => 1,  
                'first_name' => 'Mohamed',
                'last_name' => 'Fathy',
                'phone' => '01008292985',
                'latitude' => '23.241346',
                'longtiude' => '28.652344',
                'address' => '10 mourad bek',
                'addtional_address' => '12 mourad bek', 
                'user_id' => 1,
                'address_lable' => 'home',
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40'
            ],
            [
                'id' => 2,  
                'first_name' => 'Islam',
                'last_name' => 'Shaban',
                'phone' => '01008292666',
                'latitude' => '23.241333',
                'longtiude' => '28.632333',
                'address' => '20 mourad bek',
                'addtional_address' => '25 mourad bek', 
                'user_id' => 2,
                'address_lable' => 'home',
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40'
            ],

        ];

        DB::table('shippings')->insert($shipping);
    }
}
