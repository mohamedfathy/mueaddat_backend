<?php

use Illuminate\Database\Seeder;

class ProductOptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product_options = [
            [
                'id' => 1,  
                'product_id' => 1,
                'option_id' => 1,
                'option_group_id' => 1,
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],
            [
                'id' => 2,  
                'product_id' => 1,
                'option_id' => 2,
                'option_group_id' => 2,
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],
            [
                'id' => 3,  
                'product_id' => 1,
                'option_id' => 3,
                'option_group_id' => 3,
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],

        ];


        DB::table('product_options')->insert($product_options);
    }
}
