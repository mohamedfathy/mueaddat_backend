<?php

use Illuminate\Database\Seeder;

class OptionsGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $options_group = [
            [
                'id' => 1,   
                'option_group_name_ar' => 'المقاس',
                'option_group_name_en' => 'size',
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],
            [
                'id' => 2,   
                'option_group_name_ar' => 'الموديل',
                'option_group_name_en' => 'model',
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],
            [
                'id' => 3,  
                'option_group_name_ar' => 'القدرة',
                'option_group_name_en' => 'power',
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ]
        ];


        DB::table('option_groups')->insert($options_group);
    }
}
