<?php

use Illuminate\Database\Seeder;

class ProductReviewsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $reviews = [
            [
                'id' => 1,
                'review' => 5,
                'detail' => 'first review',
                'product_id' => 1,
                'user_id' => 1,
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],
            [
                'id' => 2,
                'review' => 4,
                'detail' => 'second review',
                'product_id' => 1,
                'user_id' => 4,
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],
            [
                'id' => 3,
                'review' => '5',
                'detail' => 'third review',
                'product_id' => 2,
                'user_id' => 2,
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],
        ];

        DB::table('product_reviews')->insert($reviews);
    }
}
