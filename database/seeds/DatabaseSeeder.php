<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            BillingTableSeeder::class,
            BrandsTableSeeder::class,
            CategoriesTableSeeder::class,
            FeaturedTableSeeder::class,
            OptionsGroupsTableSeeder::class,
            OptionsTableSeeder::class,
            OrdersDetailsTableSeeder::class,
            OrdersTableSeeder::class,
            ProductImagesTableSeeder::class,
            ProductOptionsTableSeeder::class,
            ProductReviewsTableSeeder::class,
            ProductsTableSeeder::class,
            ShippingTableSeeder::class,
            SliderTableSeeder::class,
            UsersTableSeeder::class,
        ]
        );
    }
}
