<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'id' => 1,  
                'first_name' => 'Mohamed',
                'last_name' => 'Fathy',
                'email' => 'torbedosquadron8@gmail.com',
                'email_verified_at' => NULL,
                'type' => 'supplier', 
                'phone' => '01008292985',
                'city' => 'Cairo',
                'country' => 'Egypt',
                'password' => bcrypt('secret'),
                'remember_token' => NULL,
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40'
            ],
            [
                'id' => 2,  
                'first_name' => 'Islam',
                'last_name' => 'Shaban',
                'email' => 'islamshaban@gmail.com',
                'email_verified_at' => NULL,
                'type' => 'user', 
                'phone' => '01008296666',
                'city' => 'Minya',
                'country' => 'Egypt',
                'password' => bcrypt('secret'),
                'remember_token' => NULL,
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40'
            ],

        ];


        DB::table('users')->insert($users);
    }
}
