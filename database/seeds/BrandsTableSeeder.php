<?php

use Illuminate\Database\Seeder;

class BrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brands = [
            [
                'id' => 1,
                'name_ar' => 'اي بى تى',
                'name_en' => 'apt',
                'image_url' => 'uploads/brands/apt.jpeg',
                'is_active' => 1,
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],
            [
                'id' => 2,
                'name_ar' => 'بلاك بلس ديكر',
                'name_en' => 'black + deaker',
                'image_url' => 'uploads/brands/black_plus_decker.png',
                'is_active' => 1,
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],
            [
                'id' => 3,
                'name_ar' => 'بوش',
                'name_en' => 'bosch',
                'image_url' => 'uploads/brands/bosch.png',
                'is_active' => 1,
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],
            [
                'id' => 4,
                'name_ar' => 'كروان',
                'name_en' => 'crown',
                'image_url' => 'uploads/brands/crown.png',
                'is_active' => 1,
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],
            [
                'id' => 5,
                'name_ar' => 'ديولت',
                'name_en' => 'dewalt',
                'image_url' => 'uploads/brands/dewalt.png',
                'is_active' => 1,
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],
            [
                'id' => 6,
                'name_ar' => 'دريميل',
                'name_en' => 'dremel',
                'image_url' => 'uploads/brands/dremel.png',
                'is_active' => 1,
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],
            [
                'id' => 7,
                'name_ar' => 'دي دبليو تى',
                'name_en' => 'dwt',
                'image_url' => 'uploads/brands/dwt.png',
                'is_active' => 1,
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],
            [
                'id' => 8,
                'name_ar' => 'فيستول',
                'name_en' => 'festool',
                'image_url' => 'uploads/brands/festool.png',
                'is_active' => 1,
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],
            [
                'id' => 9,
                'name_ar' => 'هاكوكي',
                'name_en' => 'hikoki',
                'image_url' => 'uploads/brands/hikoki.jpg',
                'is_active' => 1,
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],
            [
                'id' => 10,
                'name_ar' => 'هيلتى',
                'name_en' => 'hilti',
                'image_url' => 'uploads/brands/hilti.png',
                'is_active' => 1,
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],
            [
                'id' => 11,
                'name_ar' => 'انكو',
                'name_en' => 'ingco',
                'image_url' => 'uploads/brands/ingco.png',
                'is_active' => 1,
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],
            [
                'id' => 12,
                'name_ar' => 'ماكيتا',
                'name_en' => 'makita',
                'image_url' => 'uploads/brands/makita.png',
                'is_active' => 1,
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],
            [
                'id' => 13,
                'name_ar' => 'ميتابو',
                'name_en' => 'meatbo',
                'image_url' => 'uploads/brands/metabo.jpg',
                'is_active' => 1,
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],
            [
                'id' => 14,
                'name_ar' => 'سكيل',
                'name_en' => 'skil',
                'image_url' => 'uploads/brands/skil.png',
                'is_active' => 1,
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],
            [
                'id' => 15,
                'name_ar' => 'استانلى',
                'name_en' => 'stanley',
                'image_url' => 'uploads/brands/stanley.png',
                'is_active' => 1,
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ],
            [
                'id' => 16,
                'name_ar' => 'توتال',
                'name_en' => 'total',
                'image_url' => 'uploads/brands/total.png',
                'is_active' => 1,
                'created_at' => '2020-02-26 15:46:40',
                'updated_at' => '2020-02-26 15:46:40',
                'deleted_at' => NULL,
            ]
        ];

        DB::table('brands')->insert($brands);
    }
}
