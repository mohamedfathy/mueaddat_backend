<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id');
            $table->bigInteger('billing_id');
            $table->bigInteger('shipping_id');	
            $table->date('shipping_date');	
            $table->date('shipper_id');	
            $table->decimal('shipping_cost', 10, 2);
            $table->decimal('total_cost', 10, 2);
            $table->enum('status', ['in_process', 'canceled', 'shipped', 'delivered']);
            $table->date('paid');	
            $table->date('payment_date');		
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();	
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
