<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('stock_keeping_unit', 50);
            $table->string('supplier_stock_keeping_unit', 50);
            $table->string('name_ar', 100);
            $table->string('name_en', 100);
            $table->string('description_ar', 255);
            $table->string('description_en', 255);
            $table->bigInteger('supplier_id');
            $table->bigInteger('category_id');
            $table->bigInteger('quantity_per_unit');
            $table->string('unit_size', 20);
            $table->string('unit_price', 20);
            $table->decimal('suggested_price', 10, 2);
            $table->string('size', 50)->nullable();
            $table->string('color', 50)->nullable();
            $table->smallInteger('discount');
            $table->mediumInteger('unit_weight')->nullable();
            $table->smallInteger('units_in_stock');	
            $table->smallInteger('units_on_order');	
            $table->smallInteger('reorder_level');
            $table->boolean('product_available');
            $table->boolean('discount_available');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();	
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
