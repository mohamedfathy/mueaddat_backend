<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/getLanguage', 'API\APIsController@getLanguage');
Route::post('/getMainCategories', 'API\APIsController@getMainCategories');
Route::post('/getSliders', 'API\APIsController@getSliders');
Route::post('/getFeatured', 'API\APIsController@getFeatured');
Route::post('/getBrands', 'API\APIsController@getBrands');


Route::get('/search', 'ProductController@search');
