<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>Mueaddat</title>
    <link href=" {{ mix('css/app.css') }}" rel="stylesheet">

    <!-- go to https://www.favicon-generator.org/ to create favicon -->
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="assets/css/vendor/bootstrap_v4.4.1.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <!-- english font-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700&display=swap" rel="stylesheet">
    <!-- arabic font-->
    <link href="https://fonts.googleapis.com/css?family=Tajawal:400,500,700&display=swap" rel="stylesheet">
        <!-- fontawesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
    integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
  </head>
  <body>
    <div id="app">
        <app></app>
    </div>

    <script src="{{ mix('js/app.js') }}"></script>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="assets/js/vendor/jquery_v3.4.1.js"></script>
    <script src="assets/js/vendor/popper_v1.6.0.js"></script>
    <script src="assets/js/vendor/bootstrap_v4.4.1.js"></script>
    <script src="assets/js/main.js"></script>
  </body>
</html>
