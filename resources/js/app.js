// import bootstrap
require('./bootstrap');

// import the vue , vue-router
import Vue from 'vue'
import VueRouter from 'vue-router'

// using the dependencies
Vue.use(VueRouter);

// import the components
import app from './components/App.vue';
import HomeComponent from './components/HomeComponent.vue';

// construct the router
const router = new VueRouter({ 
    mode: 'history',
    routes: [
        {
            name: 'home',
            path: '/',
            component: HomeComponent
        },
    ]
});

// initialize the application
new Vue({
    router,
    render: h => h(app)
}).$mount('#app');